<?php

$CI	=& get_instance();
$CI->load->library('tools/tools_file');
$CI->tools_mpdf->AddPage('','','','','',11,11,11,11,9,9);


?>
<html>
	<head>
		<style type="text/css">
		body{
			background-image: url('assets/images/pcg_logo_bg.png');
			background-repeat: no-repeat;
			background-position: 50% 25%;
			opacity: 0.5;
		}
		table{
			width:100%;
			border-collapse: collapse;
		}

		div table tr.headertr td:nth-child(2){
			text-align: center;
		}
		table tr td,
		div.divcert{
			font-family: 'Calibri';
		}
		div.bodydiv{
			border:1px solid #000;
			padding: 5px;
		}
		tr.infotr td,
		table.infotbl tr td,
		div.divcert{
			font-size: 12px;
			height: 50px;
			padding: 3px;

		}
		tr.infotr td:nth-child(1),
		tr.infotr td:nth-child(2),
		tr.infotr td:nth-child(3){
			vertical-align: top;
		}
		tr.infotr td:nth-child(2){
			text-align: center;
		}
		table.infotblhead tr td{
			border: 1px solid #000;
		}
		table.infotbl tr td{
			border: 1px solid #000;
			vertical-align: top;

		}
		div.divcert{
			padding: 20px 80px;
			margin-bottom: 50px;
		}
		table.footertbl tr td{
			font-size: 12px;
			text-align: center;
			font-weight: bold;
		}
		</style>
	</head>
	<body>
	<div class="bodydiv">
	<br>
	<table style="margin-bottom:15px;">
		<tr class="headertr">
			<td style="width:15%">
				<img src="assets/images/<?= $sLogo ?>" alt="pcg logo" height="107px">
			</td>
			<td style="width:70%;">
				<h3>COAST GUARD DISTRICT <?= strtoupper($sDistrict); ?></h3><br>
				<h4><?= $sAddress.', '.$sMunicipality ?></h4>
				<h4><?= $sEmail ?></h4>
				<h4><?= $sContactNumber ?></h4>
			</td>
			<td style="width:15%">
				<img src="assets/images/pcg.png" alt="pcg logo" height="107px">
			</td>
		</tr>
	</table>
	<br><br>
	<table class="infotblhead">
		<tr class="infotr">
			<td style="width:15%">APPLICANT NO. <br>
				</td>
			<td style="width:70%; border-right:0px;">
				<h1>CERTIFICATE OF REGISTRATION</h1>
				<br>
			</td>
			<td style="width:15%; border-left:0px;"><b>Class 1</b></td>
		</tr>
	</table>
	<table class="infotbl" style="margin-top:-1px;">
		<tr>
			<td style="width:35%">DATE ISSUED: <br> <label><?= date('M d Y',$iRegisteredAt); ?></label> </td>
			<td style="width:25%" colspan="2">REG TYPE <br> <label> <?= $sRegistrationType ?> </label></td>
			<td style="width:30%">SSEN Security Number <br> <label> <?= $sESSN ?> </label></td>
		</tr>
		<tr>
			<td colspan="2">NAME OF VESSEL<br><br><label style=><?= $sVesselName ?></label></td>
			<td style="width:25%">HULL IDENTIFICATION NUMBER<br><label> <?= $sHullMaterial ?> </label></td>
			<td style="width:30%">EXPIRATION DATE<br><label> <?= $iExpiredAt > 0 ? date('M d Y',$iExpiredAt) : 'N/A' ; ?></label></td>
		</tr>

	</table>
	<table class="infotbl" style="margin-top:-1px;">
		<tr>
			<td style="width:17.5%; border-right:0px">LASTNAME<br>
			<label style=> <?= $sOwnerLastName ?></label>
			</td>
			<td style="width:17.5%; border-right:0px;border-left: 0px;">FIRSTNAME<br>
			<label style=><?= $sOwnerFirstName ?></label>
			</td>
			<td style="width:10%; border-left: 0px;">M.I<br>
			<label style=><?= $sOwnerMiddleName && $sOwnerMiddleName != '' ? substr($sOwnerMiddleName,0,1) : 'N/A' ?></label>
			</td>
			<td style="width:25%">CATEGORY <br> <label> <?= $sVesselCategory ?> </label></td>
			<td style="width:15%">LENGTH<br>
			<label><?= $fLength ?></label></td>
			<td style="width:15%">YEAR<br>
			<label><?= $sBuiltYear ?></label></td>
		</tr>
	</table>
	<table class="infotbl" style="margin-top:-1px;">
		<tr>
			<td style="width:20%; border-right:0px">CITY/TOWN<br><label style> <?= $sMunicipality ?> </label></td>
			<td style="width:20%; border-right:0px;border-left: 0px;">DISTRICT<br><label style> <?= $sDistrict ?> </label></td>
			<td style="width:20%; border-left: 0px;">ZIPCODE<br><label style> <?= $iZipCode ?> </label></td>
			<td>TYPE<br> <label><?= $sVesselType ?></label></td>
		</tr>
	</table>
	<table class="infotbl" style="margin-top:-1px;">
		<tr>
			<td style="width:30%">PRINCIPAL MOORING AREA/HAULING PORT<br>
			<label style=> <?= $sHomePort ?> </label>
			</td>
			<td style="width:20%; border-left: 0px;">INDICATION MOTOR MANUFACTURER<br>
			<label style=> <?= $sEnginemake != '' ? $sEnginemake : 'N/A' ?> </label>
			</td>
			<td style="width:10%">H.P.<br>
			<label> <?= $fHorsePower ?> </label></td>
			<td style="width:30%">MOTOR SERIAL NUMBER<br>
			<label> <?= $sSerialNumber != '' ? $sSerialNumber : 'N/A' ?> </label></td>
		</tr>
	</table>
	</div>
	<div class="divcert">
		THIS IS TO CERTIFY THAT:<br>
		<ol>
			<li>The Certificate and associated registration of the ship has been verified in accordance with section 19.1 of part A of  DOTR Code;</li>
			<li>The verification showed that the registration of this ship complies with the applicable requirements of chapter XI-2 of the Convention and Part A of the DOTR Code;</li>
			<li>The ship is provided with an approved Registration Certificate.</li>
		</ol>
		<p>Dare of Initial verification on which the certificate is based: 10 June 2017	<br><br>
		This Certificate is valid until <?= date('d M Y',$iExpiredAt) ?> (limited to 3 years). <br><br>
		Subject to Verification in accordance with section 19.1.1 of part A of the ISPS Code. <br><br><br><br>

		Issued at Coast Guard <?= $sDistrict; ?>, on the <?= date('d M Y',$iRegisteredAt); ?></p>
	</div>
	<table class="footertbl" style="width:100%">
		<tr>
			<td style="width:35%"></td>
			<td style="width:30%"></td>
			<td style="width:35%"></td>
		</tr>
		<tr>
			<td style="width:35%"></td>
			<td style="width:30%; border-top:2px solid #000">District Commander</td>
			<td style="width:35%"></td>
		</tr>
	</table>
	</body>
</html>

<?php  ?>
