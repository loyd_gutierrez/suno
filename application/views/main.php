<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <title>Suno | Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="assets/fonts/font_awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="assets/fonts/ion_icons/css/ionicons.min.css">
  <!-- Datatables -->
  <link rel="stylesheet" href="assets/plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="assets/plugins/adminLTE/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="assets/plugins/adminLTE/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="assets/plugins/iCheck/flat/blue.css">
   <!-- Select2 -->
  <link rel="stylesheet" href="assets/plugins/select2/select2.min.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- Toastr style -->
  <link rel="stylesheet" href="assets/plugins/toastr-master/build/toastr.min.css">
  <!--  -->
  <link rel="stylesheet" href="assets/css/style.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-black sidebar-mini">
  <div class="loader" style="display:none;">
    <div class="overlay">
      <i class="fa fa-refresh fa-spin"></i>
    </div>
  </div>
  <div class="login-box view" name="login">
    <div class="login-logo">
      <h3>Suno | Admin</h3>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <form name="login" method="post" onsubmit="return false">
        <div class="form-group has-feedback">
          <input name = "username" type="text" class="form-control" placeholder="Email Address">
          <span class="fa fa-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input name = "password" type="password" class="form-control" placeholder="Password">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
          <div class="col-xs-8">
            <!-- <a id="signup_btn" class="pull-left " href="javascript:void(0)">  Sign up for new account</a> -->
          </div>
          <!-- /.col -->
          <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.login-box-body -->
  </div>
  <!-- /.login-box -->

  <!-- Wrapper -->
  <div class="wrapper view" name="app">
      <header class="main-header">
        <!-- Logo -->
        <a href="javascript:void(0)" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"> SUNO </span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"> <img src ="assets/images/suno.png"></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
              <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
              <ul class="nav navbar-nav">
                <li class="user user-menu"> <a name="btn_logout" href="javascript:void(0)" class="btn btn-flat" style="padding-left:25px;padding-right:25px;"> <i class="fa fa-sign-out"></i></a></li>
              </ul>
            </div>
        </nav>
      </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <!-- Sidebar user panel -->
        <ul class="sidebar-menu">
          </ul>
      </section>
      <!-- /.sidebar -->
    </aside>

      <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <div class="module-content">
        <div class="col-xs-12">
          <div class="lists"></div>
          <div class="forms"></div>
        </div>
      </div>
    </div>
      <!-- /.content-wrapper -->
    <footer class="main-footer">
      <strong>Copyright &copy; 2017.</strong> All rights reserved.
    </footer>

    <!-- Modal -->
    <div name="Confirm" class="modal fade DataTableModal" role="dialog">
      <div class="modal-dialog">
        <form class="modal-content" onsubmit="return false">
          <input type="hidden" name="id">
          <div class="modal-header customhdr">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
            <h4 class="modal-title">Confirmation</h4>
          </div>
          <div class="modal-body"></div>
          <div class="modal-footer customdlft">
            <button name="Cancel" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button name="Confirm" type="button" class="btn btn-primary">Confirm</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- ./Wrapper -->




  <!-- jQuery 2.2.3 -->
  <script src="assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="assets/plugins/jQueryUI/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  <!-- Datatables -->
  <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
  <!-- Select2 -->
  <script src="assets/plugins/select2/select2.min.js"></script>
  <!-- date-time-picker -->
  <script src="assets/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
  <!-- daterangepicker -->
  <script src="assets/plugins/daterangepicker/moment.min.js"></script>
  <script src="assets/plugins/daterangepicker/daterangepicker.js"></script>
  <!-- datepicker -->
  <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
  <!-- Bootstrap WYSIHTML5 -->
  <script src="assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
  <!-- Slimscroll -->
  <script src="assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="assets/plugins/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="assets/plugins/adminLTE/js/adminLTE.min.js"></script>
  <!-- Sortable -->
  <script src="assets/plugins/jquery-sortable/jquery-sortable-min.js"></script>
  <!-- Validation -->
  <script src="assets/plugins/validation/dist/jquery.validate.min.js"></script>
  <!-- Toastr -->
  <script src="assets/plugins/toastr-master/build/toastr.min.js"></script>

  <!-- Applications script -->
  <script src="assets/js/app.js"></script>
  <script src="assets/js/oPhp.js"></script>

</body>
</html>
