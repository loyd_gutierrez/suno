<div name="Account" class="row" role="list" style="display: none;">
	<div class="col-xs-12">
		<div class="box module-content">
			<div class="box-header">
				<h3 class="box-title"><i class="fa fa-users"> </i> Account</h3>
				<div class="box-tools pull-right">
					<span data-toggle="tooltip" title="" class="badge bg-green"></span>
					<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body">
				<div class="box">
					<div class="box-body">
						<table name="Account_DataTable"  class="table table-striped table-hover" width="100%"></table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
