<div name="Settings" class="row" role="list" style="display: none;">
	<div class="col-xs-12">
		<div class="box module-content">
			<div class="box-header">
				<h3 class="box-title"><i class="fa fa-gears"> </i> Settings</h3>
				<div class="box-tools pull-right">
					<span data-toggle="tooltip" title="" class="badge bg-green"></span>
					<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body">
				<div class="box">
					<div class="box-body">
						<h3>Admin Credentials </h3>
						<hr />
						<div class="col-md-12">
							<div class="form-group">
								<div class="col-sm-3">
									<label class="control-label"><i class="fa"></i> Username</label>
								</div>
								<div class="col-sm-7">
									<input name="username" placeholder="Username" class="form-control" type="text" autocomplete="off" readonly>
								</div>
							</div>

							<br />
							<br />

							<div class="form-group">
								<div class="col-sm-3">
									<label class="control-label"><i class="fa"></i> Password</label>
								</div>
								<div class="col-sm-7">
									<input name="password" placeholder="Password" class="form-control" type="password" readonly>
								</div>
							</div>
						</div>
						<br />
						<br />	
						<h3> Rate Settings </h3>
						<hr />
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<div class="col-sm-3">
										<label class="control-label"><i class="fa"></i>  Base Rate </i> </label>
									</div>
									<div class="col-sm-7">
										<input name="minimumRate" placeholder="Base rate" class="form-control" type="text" autocomplete="off" readonly>
									</div>
								</div>

								<br />
								<br />

								<div class="form-group">
									<div class="col-sm-3">
										<label class="control-label"><i class="fa"></i> Price / KM</label>
									</div>
									<div class="col-sm-7">
										<input name="perKM" placeholder="Price per KM" class="form-control" type="text" readonly>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<button name="editButton" action="edit" type="submit" class="btn btn-primary pull-right"> Change </button>
			</div>
		</div>
	</div>
</div>
