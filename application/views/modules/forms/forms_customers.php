<div name="Customers" path="Customers/Customers" class="modal fade DataTableModal" role="dialog" style="display: none;">
	<div class="modal-dialog W1000">
		<form class="modal-content" onsubmit="return false">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
				<h4 class="modal-title"><span></span> Customer </h4>
			</div>
			<div class="modal-body with-border">
				<div class="form-horizontal">
					<input name="id" type="hidden">
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title"> Account Information </h3>
						</div>
						<div class="box-body">
							<div class="col-sm-12">
								<div class="form-group">
									<div class="col-sm-5">
										<label class="control-label"><i class="fa"></i> Username</label>
										<input name="username" placeholder="Username" class="form-control" type="text" autocomplete="off">
									</div>
									<div class="col-sm-5">
										<label class="control-label"><i class="fa"></i> Password</label>
										<input name="password" placeholder="Password" class="form-control" type="password">
									</div>
									<div class="col-sm-2 pull-right">
										<label class="control-label"><i class="fa"></i> Active</label><br>
										<select name="is_active" field-type="Select" class="form-control" style="width:100%">
											<option value="1">Active</option>
											<option value="0">Inactive</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-4">
										<label class="control-label"><i class="fa"></i> Last Name</label>
										<input name="last_name" type="text" class="form-control" placeholder="Last Name">
									</div>
									<div class="col-sm-4">
										<label class="control-label"><i class="fa"></i> First Name</label>
										<input name="first_name" type="text" class="form-control" placeholder="First Name">
									</div>
									<div class="col-sm-4">
										<label class="control-label"><i class="fa"></i> Middle Name</label>
										<input name="middle_name" type="text" class="form-control" placeholder="Middle Name">
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="user_input">
						<div class="box">
							<div class="box-header with-border">
								<h3 class="box-title"> Personal Information </h3>
							</div>
							<div class="box-body">
								<div class="form-group">
								<div class="col-sm-4">
									<label class="control-label"><i class="fa"></i> Gender </label>
									<select field-type="Select" name="gender" class="form-control select2" style="width:100%">
										<option value="m"> Male </option>
										<option value="f"> Female </option>
									</select>
								</div>
								<div class="col-sm-4">
									<label class="control-label"><i class="fa"></i> Birth Date </label>
									<input name="birthdate" field-type="Date" type="text" class="form-control" placeholder="">
								</div>
								<div class="col-sm-4">
									<label class="control-label"><i class="fa"></i> Contact Number </label>
									<input name="contact_number" type="text" class="form-control" placeholder="">
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<div class="modal-footer">
				<button action="cancel" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button action="save" type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</form>
	</div>
</div>
