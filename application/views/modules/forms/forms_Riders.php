<div name="Riders" path="Riders/Riders" class="modal fade DataTableModal" role="dialog" style="display: none;">
	<div class="modal-dialog W1000">
		<form class="modal-content" onsubmit="return false">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
				<h4 class="modal-title"><span></span> Riders</h4>
			</div>
			<div class="modal-body with-border">
				<div class="form-horizontal">
					<input name="id" type="hidden">
					<div class="form-group">
						<div class="col-sm-3 pull-right">
							<label class="control-label col-sm-3"><i class="fa"></i> Active</label><br>
							<div class="col-sm-9">
								<select name="is_active" field-type="Select" class="form-control" style="width:100%">
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								</select>
							</div>
						</div>
					</div>
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title"> Account Information </h3>
						</div>
						<div class="box-body">
							<div class="col-sm-3 fa thumbnail upload">
								<img name="photo" field-type="Photo" class="img-rounded" alt="&#xf007;">
								<input field-type="Image" field-size="140x140" name="photo_fPhoto" type="file" action="upload" style="display: none;">
							</div>

							<div class="col-sm-9">
								<div class="form-group">
									<div class="col-sm-4">
										<label class="control-label"><i class="fa"></i> Username</label>
										<input name="username" placeholder="Username" class="form-control" type="text" autocomplete="off">
									</div>
									<div class="col-sm-4">
										<label class="control-label"><i class="fa"></i> Password</label>
										<input name="password" placeholder="Password" class="form-control" type="password">
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-4">
										<label class="control-label"><i class="fa"></i> Last Name</label>
										<input name="last_name" type="text" class="form-control" placeholder="Last Name">
									</div>
									<div class="col-sm-4">
										<label class="control-label"><i class="fa"></i> First Name</label>
										<input name="first_name" type="text" class="form-control" placeholder="First Name">
									</div>
									<div class="col-sm-4">
										<label class="control-label"><i class="fa"></i> Middle Name</label>
										<input name="middle_name" type="text" class="form-control" placeholder="Middle Name">
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="user_input">
						<div class="box">
							<div class="box-header with-border">
								<h3 class="box-title"> Personal Information </h3>
							</div>
							<div class="box-body">
								<div class="form-group">
								<div class="col-sm-4">
									<label class="control-label"><i class="fa"></i> Gender </label>
									<select field-type="Select" name="gender" class="form-control select2" style="width:100%">
										<option value="m"> Male </option>
										<option value="f"> Female </option>
									</select>
								</div>
								<div class="col-sm-4">
									<label class="control-label"><i class="fa"></i> Birth Date </label>
									<input name="birthdate" field-type="Date" type="text" class="form-control" placeholder="">
								</div>
								<div class="col-sm-4">
									<label class="control-label"><i class="fa"></i> Contact Number </label>
									<input name="contact_number" type="text" class="form-control" placeholder="">
								</div>
							</div>
						</div>
					</div>
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title"> Motor Information </h3>
						</div>
						<div class="box-body">
							<div class="form-group">
								<div class="col-sm-3 fa thumbnail upload">
									<img name="photo_motor" field-type="Photo" class="img-rounded" alt="&#xf007;">
									<input field-type="Image" field-size="140x140" name="photo_motor_fPhoto" type="file" action="upload" style="display: none;">
								</div>
								<div class="col-sm-3">
									<label class="control-label"><i class="fa"></i> Plate Number </label>
									<input name="plate_number" type="text" class="form-control" placeholder="">
								</div>
								<div class="col-sm-3">
									<label class="control-label"><i class="fa"></i> License Number </label>
									<input name="license_number" type="text" class="form-control" placeholder="">
								</div>
								<div class="col-sm-3">
									<label class="control-label"><i class="fa"></i> OR/CR </label>
									<input name="or_cr" type="text" class="form-control" placeholder="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button action="cancel" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button action="save" type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</form>
	</div>
</div>
