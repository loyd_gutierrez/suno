<div name="Settings" path="Settings/Settings" class="modal fade DataTableModal" role="dialog" style="display: none;">
	<div class="modal-dialog W1000">
		<form class="modal-content" onsubmit="return false">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
				<h4 class="modal-title"><span></span> Settings</h4>
			</div>
			<div class="modal-body with-border">
				<div class="box-body">
					<h3>Admin Credentials </h3>
					<hr />
					<div class="col-md-12">
						<div class="form-group">
							<div class="col-sm-3">
								<label class="control-label"><i class="fa"></i> Username</label>
							</div>
							<div class="col-sm-7">
								<input name="username" placeholder="Username" class="form-control" type="text" autocomplete="off" >
							</div>
						</div>

						<br />
						<br />

						<div class="form-group">
							<div class="col-sm-3">
								<label class="control-label"><i class="fa"></i> Password</label>
							</div>
							<div class="col-sm-7">
								<input name="password" placeholder="Password" class="form-control" type="password" >
							</div>
						</div>
					</div>
					<br />
					<br />
					<h3> Rate Settings </h3>
					<hr />
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<div class="col-sm-3">
									<label class="control-label"><i class="fa"></i>  Base Rate </i> </label>
								</div>
								<div class="col-sm-7">
									<input name="minimumRate" placeholder="Base rate" class="form-control" type="text" autocomplete="off" >
								</div>
							</div>

							<br />
							<br />

							<div class="form-group">
								<div class="col-sm-3">
									<label class="control-label"><i class="fa"></i> Price / KM</label>
								</div>
								<div class="col-sm-7">
									<input name="perKM" placeholder="Price per KM" class="form-control" type="text" >
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button action="cancel" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button action="save" type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</form>
	</div>
</div>
