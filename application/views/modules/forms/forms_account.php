<div name="Account" path="Account/Account" class="modal fade DataTableModal" role="dialog" style="display: none;">
	<div class="modal-dialog W1000">
		<form class="modal-content" onsubmit="return false">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
				<h4 class="modal-title"><span></span> Account</h4>
			</div>
			<div class="modal-body with-border">
				<div class="form-horizontal">
					<input name="id" type="hidden">

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<div class="col-sm-6">
									<label class="control-label"><i class="fa"></i> Username</label>
									<input name="username" placeholder="Username" class="form-control" type="text" autocomplete="off">
								</div>
								<div class="col-sm-6">
									<label class="control-label"><i class="fa"></i> Password</label>
									<input name="password" placeholder="Password" class="form-control" type="password">
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<div class="modal-footer">
				<button action="cancel" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button action="save" type="submit" class="btn btn-primary">Save Changes</button>
			</div>
		</form>
	</div>
</div>
