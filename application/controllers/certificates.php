<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @name API controller
 * @author      JL Gutierrez <lloydgutierez24@gmail.com>
 * @version		1.0.0
 */

class Certificates extends CI_Controller
{
	private $aError_codes = [
		'401' 	=> 'Access Denied',
		'401.1' => 'Login Error',
		'403' 	=> 'Forbidden Access',
		'404' 	=> 'Request not found'

	];

	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods','GET');
		header('conten-type:json');

		parent::__construct();

		$this->generate();
	}

	private function generate(){

		$aRecord = [];

		$sMethod = 'db_main_vessels';
		$sId = $this->uri->segment(3);

		if(  !class_exists('mpdf') ) {
			$this->load->model('mpdf');
		}

		$this->load->library('db/main/'.$sMethod);
		$this->load->library('db/main/db_main_users');

		$aRecord = $this->$sMethod->get($sId)['data'][0];
		$this->mpdf->print_certificate(strtolower($this->uri->segment(2)),$aRecord);
	}
}