<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @name API controller
 * @author      JL Gutierrez <lloydgutierez24@gmail.com>
 * @version		1.0.0
 */

class Archives extends CI_Controller
{
	private $aError_codes = [
		'401' 	=> 'Access Denied',
		'401.1' => 'Login Error',
		'403' 	=> 'Forbidden Access',
		'404' 	=> 'Request not found'

	];

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('file');
		
		$sMethod = 'db_main_archives';

		$this->load->library('db/main/'.$sMethod);
		$sId = $this->uri->segment(2);
		$aRecord = $this->$sMethod->get($sId)['data'];

		$this->getFile($aRecord);
	}

	private function getFile($aRecord){
		if(!$aRecord) {
			return $this->load->view('forbidden');
		}

		$mime = get_mime_by_extension(PATH_ARCHIVES.$aRecord[0]['file'] . '.' . $aRecord[0]['fileext']);
		$fileName = $aRecord[0]['title'].'.'.$aRecord[0]['fileext'];
		$filepath = PATH_ARCHIVES.$aRecord[0]['file'] . '.' . $aRecord[0]['fileext'];

		header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
	    header("Cache-Control: public"); // needed for internet explorer
	    header("Content-Type: ".$mime);
	    header("Content-Transfer-Encoding: Binary");
	    header("Content-Length:".filesize($filepath));
	    header("Content-Disposition: attachment; filename='$fileName'");

		readfile($filepath);
		exit;
	}












}
