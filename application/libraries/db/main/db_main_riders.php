<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_main_riders extends Db_core
{
	public $CI 			= null;

	protected $sModule  = '';
	protected $sTable 	= 'users';
	protected $sIndex 	= 'id';

	protected $aColumns = array(
		'id'					=> 'users.id',

		'role_id'				=> 'users.role_id role_id',

		'username'				=> 'users.username',
		'password'				=> 'users.password',
		'photo'					=> 'users.photo',

		'last_name'				=> 'users.last_name',
		'first_name'			=> 'users.first_name',
		'middle_name'			=> 'users.middle_name',
		'gender'				=> 'users.gender',
		'birthdate'				=> 'users.birthdate',
		'contact_number'		=> 'users.contact_number',
		'license_number'		=> 'users.license_number',
		'plate_number'			=> 'users.plate_number',
		'or_cr'					=> 'users.or_cr',
		'photo_motor'			=> 'users.photo_motor',
		'fullname'				=> 'CONCAT( users.last_name, CONCAT(" "), users.first_name ) fullname',

		'role'					=> 'roles.role',
	);

	protected $aFields = array(
		'id',
		'role_id',

		'username',
		'password',
		'last_name',
		'first_name',
		'middle_name',
		'gender',
		'birthdate',
		'contact_number',
		'photo',
		'photo_motor',
		'license_number',
		'plate_number',
		'or_cr'
	);

	function __construct(){
		$this->CI =& get_instance();

		$this->CI->load->library('db/main/db_main_roles');
		$this->CI->load->library('db/main/db_main_users');
	}

	public function get($sId = null ,$aArg = array())
	{
		$this->CI->db->select($this->select($aArg, array_merge(
			$this->aColumns,
			array(
				'id'					=> 'users.id',
				'is_active'				=> 'users.is_active',
				'created_at'			=> 'users.created_at',
				'updated_at'			=> 'users.updated_at'
			)
		)));

		$this->CI->db->from($this->sTable);

		$this->CI->db->join('roles','roles.id = users.role_id  AND roles.is_active = 1','left');

		$this->CI->db->where('roles.role = "Rider"');
		$sId && $this->CI->db->where('users.id = '.$sId);

		$this->search($aArg);

		$oQuery = clone $this->CI->db;
		$oQuery = $oQuery->get();

		$iRecordsFiltered = $oQuery->num_rows();
		$oQuery->free_result();

		$this->order($aArg);
		$this->limit($aArg);

		$aData 		= $this->CI->db->get();
		$aReturn 	= $aData->result_array();

		return array_merge(
			[
				'recordsFiltered' => $iRecordsFiltered,
				'recordsTotal'    => $this->CI->db->count_all($this->sTable)
			],
			array('data' => $this->getResult($aReturn,$aArg))
		);
	}

	public function add($id = null, $aArg)
	{
		if($this->CI->db_main_users->get(null,['username' => $aArg['aRequest']['username']])['data']) {
			return array('iError' => 1, 'sMessage' => 'username already exist!');
		}

		$aArg = $this->password($aArg);
		$aArg['aRequest']['photo'] = $this->image($aArg);
		$aArg['aRequest']['photo_motor'] = $this->motor($aArg);
		$aArg['aRequest']['role_id'] = $this->CI->db_main_roles->get(null,['role' => 'Rider'])['data'][0]['id'];
		$aId = $this->insert($aArg['aRequest']);

		return array('sMessage' => 'Record saved!', 'id' => $aId['id']);
	}

	public function edit($id, $aArg = array())
	{
		$aArg = $this->password($aArg);
		$aArg['aRequest']['photo'] = $this->image($aArg,$this->get($id)['data'][0]['photo'],$id);
		$aArg['aRequest']['photo_motor'] = $this->motor($aArg,$this->get($id)['data'][0]['photo_motor'],$id);
		$aArg['aRequest']['role_id'] = $this->CI->db_main_roles->get(null,['role' => 'Rider'])['data'][0]['id'];

		$this->CI->db->where('users.id = '.$id);
		$this->update($aArg['aRequest']);
		print_r($this->CI->db->_error_message());
		return array('sMessage' => 'Record saved!', 'id' => $id);
	}

	private function password($aRequest = [])
	{
		if(strlen($aRequest['aRequest']['password']) > 0) $aRequest['aRequest']['password'] = md5($aRequest['aRequest']['password']);
		else unset($aRequest['aRequest']['password']);

		return $aRequest;
	}

	private function image($aArg,$sOld = null,$id = null) {

		if(!isset($aArg['aRequest']['photo']) || $aArg['aRequest']['photo'] == '') return null;

		if(isset($sOld)) {
			unlink(PATH_BASE_UPLOADS.$sOld);
		}

		if(!$id) {
			$id = rand(time(),time());
		}

		$photo = 'users/'.sha1($id);
		// save image
		$oImage = base64_decode(preg_replace('/^data:.*;base64,/i','',$aArg['aRequest']['photo']));

		$fOpen = fopen(PATH_BASE_UPLOADS.$photo,'w');
		if($fOpen) {
			file_put_contents(PATH_BASE_UPLOADS.$photo, $oImage);
			fclose($fOpen);

			return $photo;
		}

		return null;
	}
	private function motor($aArg, $sOld = null , $id = null) {
		if(!isset($aArg['aRequest']['photo_motor']) || $aArg['aRequest']['photo_motor'] == '') return null;
		if(isset($sOld)) {
			unlink(PATH_BASE_UPLOADS.$sOld);
		}

		if(!$id) {
			$id = rand(time(),time());
		}

		$photo = 'motors/'.sha1($id);
		// save image
		$oImage = base64_decode(preg_replace('/^data:.*;base64,/i','',$aArg['aRequest']['photo_motor']));

		$fOpen = fopen(PATH_BASE_UPLOADS.$photo,'w');
		if($fOpen) {
			file_put_contents(PATH_BASE_UPLOADS.$photo, $oImage);
			fclose($fOpen);

			return $photo;
		}

		return null;
	}
}
