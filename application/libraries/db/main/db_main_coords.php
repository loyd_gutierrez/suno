<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_main_coords extends Db_core
{
	public $CI 			= null;

	protected $sModule  = '';
	protected $sTable 	= 'riderscoords';
	protected $sIndex 	= 'id';

	protected $aColumns = array(
		'id'					=> 'riderscoords.id',
		'lat'					=> 'riderscoords.lat',
		'lng'					=> 'riderscoords.lng'
	);

	protected $aFields = array(
		'id',
		'lat',
		'lng'
	);

	function __construct(){
		$this->CI =& get_instance();
		$this->CI->load->library('db/main/db_main_bookings');
		$this->CI->load->library('db/main/db_main_transactions');
	}

	public function get($sId = null ,$aArg = array())
	{
		$this->CI->db->select($this->select($aArg, array_merge(
			$this->aColumns,
			array(
				'id'					=> 'riderscoords.id'
			)
		)));
		$this->CI->db->from($this->sTable);
		$sId && $this->CI->db->where('riderscoords.id = '.$sId);
		$this->search($aArg);
		$oQuery = clone $this->CI->db;
		$oQuery = $oQuery->get();
		$iRecordsFiltered = $oQuery->num_rows();
		$oQuery->free_result();
		$this->order($aArg);
		$this->limit($aArg);

		$aData 		= $this->CI->db->get();
		$aReturn 	= $aData->result_array();

		$data 		= array('data' => $this->getResult($aReturn,$aArg));

		$transaction = $this->CI->db_main_transactions->get($aArg['aRequest']['id'])['data'];
		$status = (sizeOf($transaction) > 0) ? $transaction[0]['status'] : null;
		$data['data'][0]['status'] = $status;

		if(isset($aArg['ridersNearby']) && $aArg['ridersNearby']) {
			$aTemp = [];
			foreach($data['data'] as $row) {
				if($this->getDistance($aArg['aRequest']['lat'], $aArg['aRequest']['lng'], $row['lat'], $row['lng'])){
					array_push($aTemp, $row);
				}
			}
			$data = $aTemp;
		}

		return array_merge(
			[
				'recordsFiltered' => $iRecordsFiltered,
				'recordsTotal'    => $this->CI->db->count_all($this->sTable)
			],
			$data
		);
	}

	public function add($id = null, $aArg)
	{
		if($this->get($id, $aArg)['data'][0]['id']) {
			return $this->edit($id,$aArg);
		}

		$aId = $this->insert($aArg['aRequest']);
		return $this->getBookedRequests($id,$aArg);
	}

	public function edit($id, $aArg = array())
	{
		$this->CI->db->where('riderscoords.id = '.$id);
		$this->update($aArg['aRequest']);

		return $this->getBookedRequests($id,$aArg);
	}

	public function getBookedRequests($id,$aArg){
		$status = null;
		$bookingData = [];
		if(isset($id) && $id != null) {
			$transaction = $this->CI->db_main_transactions->get(null, [
				'rider_id'	=> $id,
				'customer_id' => $aArg['aRequest']['customer_id']
			])['data'];
			$status = (sizeOf($transaction) > 0) ? $transaction[0]['status'] : null;
		}
		$bookings = $this->CI->db_main_bookings->get();
		$bookings['status'] = $status;
		$bookings['id'] = $transaction[0]['id'];

		foreach($bookings['data'] as $data) {
			if($this->getDistance($aArg['aRequest']['lat'], $aArg['aRequest']['lng'], $data['pickup_lat'], $data['pickup_lng'])){
				array_push($bookingData,$data);
			}
		}
		$bookings['data'] = $bookingData;
		return $bookings;
	}

	private function getDistance($rider_lat, $rider_lng, $client_lat, $client_lng){
		$R = 6371; // radius of earth in km
		$dLat = $this->rad($rider_lat - $client_lat);
		$dLng = $this->rad($rider_lng - $client_lng);
		$a = sin($dLat/2) * sin($dLat/2) + cos( $this->rad($rider_lat) ) * cos( $this->rad($rider_lat) ) * sin($dLng/2) * sin($dLng/2);

		$c = 2 * atan2(sqrt($a), sqrt(1-$a));
		return ($R * $c) < 0.5;
	}

	function rad($x) {return $x * M_PI/180;}
}
