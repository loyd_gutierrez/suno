<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_main_users extends Db_core
{
	public $CI 			= null;

	protected $sModule  = '';
	protected $sTable 	= 'users';
	protected $sIndex 	= 'id';

	protected $aColumns = array(
		'id'					=> 'users.id',

		'role_id'				=> 'users.role_id role_id',

		'username'				=> 'users.username',
		'password'				=> 'users.password',
		'photo'					=> 'users.photo',

		'last_name'				=> 'users.last_name',
		'first_name'			=> 'users.first_name',
		'middle_name'			=> 'users.middle_name',
		'gender'				=> 'users.gender',
		'birthdate'				=> 'users.birthdate',
		'contact_number'		=> 'users.contact_number',

		'fullname'				=> 'CONCAT( users.last_name, CONCAT(" "), users.first_name ) fullname',

		'role'					=> 'roles.role',
	);

	protected $aFields = array(
		'id',
		'role_id',

		'username',
		'password',
		'photo',
		'last_name',
		'first_name',
		'middle_name',
		'gender',
		'birthdate',
		'contact_number'
	);

	function __construct(){
		$this->CI =& get_instance();
	}

	public function get($sId = null ,$aArg = array())
	{
		$this->CI->db->select($this->select($aArg, array_merge(
			$this->aColumns,
			array(
				'id'					=> 'users.id',
				'is_active'				=> 'users.is_active',
				'created_at'			=> 'users.created_at',
				'updated_at'			=> 'users.updated_at'
			)
		)));

		$this->CI->db->from($this->sTable);

		$this->CI->db->join('roles','roles.id = users.role_id  AND roles.is_active = 1','left');

		$this->where($aArg, 'username','users');
		$this->where($aArg, 'password','users');

		$sId && $this->CI->db->where('users.id = '.$sId);

		$this->search($aArg);

		$oQuery = clone $this->CI->db;
		$oQuery = $oQuery->get();

		$iRecordsFiltered = $oQuery->num_rows();
		$oQuery->free_result();

		$this->order($aArg);
		$this->limit($aArg);

		$aData 		= $this->CI->db->get();
		$aReturn 	= $aData->result_array();

		return array_merge(
			[
				'recordsFiltered' => $iRecordsFiltered,
				'recordsTotal'    => $this->CI->db->count_all($this->sTable)
			],
			array('data' => $this->getResult($aReturn,$aArg))
		);
	}

	public function add($id = null, $aArg)
	{
		$aArg = $this->password($aArg);

		$aArg['aRequest']['photo'] = $this->image($aArg);
		$aId = $this->insert($aArg['aRequest']);

		return array('sMessage' => 'Record saved!', 'id' => $aId['id']);
	}

	public function edit($id, $aArg = array())
	{
		$aArg = $this->password($aArg);
		$aArg['aRequest']['photo'] = $this->image($aArg,$this->get($id)['data'][0]['photo'],$id);

		$this->CI->db->where('users.id = '.$id);
		$this->update($aArg['aRequest']);
		print_r($this->CI->db->_error_message());
		return array('sMessage' => 'Record saved!', 'id' => $id);
	}

	private function password($aRequest = [])
	{
		if(strlen($aRequest['aRequest']['password']) > 0) $aRequest['aRequest']['password'] = md5($aRequest['aRequest']['password']);
		else unset($aRequest['aRequest']['password']);

		return $aRequest;
	}

	private function image($aArg,$sOld = null,$id = null) {

		if(!isset($aArg['aRequest']['photo']) || $aArg['aRequest']['photo'] == '') return null;
		if(!$id) {
			$id = rand(time(),time());
		}
		if(isset($sOld)) {
			unlink(PATH_BASE_UPLOADS.$sOld);
		}

		$photo = 'users/'.sha1($id);
		// save image
		$oImage = base64_decode(preg_replace('/^data:.*;base64,/i','',$aArg['aRequest']['photo']));

		$fOpen = fopen(PATH_BASE_UPLOADS.$photo,'w');
		if($fOpen) {
			file_put_contents(PATH_BASE_UPLOADS.$photo, $oImage);
			fclose($fOpen);

			return $photo;
		}

		return null;
	}
}
