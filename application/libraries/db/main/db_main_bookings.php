<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_main_bookings extends Db_core
{
	public $CI 			= null;

	protected $sModule  = '';
	protected $sTable 	= 'bookings';
	protected $sIndex 	= 'customer_id';

	protected $aColumns = array(
		'id'					=> 'bookings.id',
		'customer_id'			=> 'bookings.customer_id',
		'rider_id'				=> 'bookings.rider_id',
		'pickup_lat'			=> 'bookings.pickup_lat',
		'pickup_lng'			=> 'bookings.pickup_lng',
		'dropoff_lat'			=> 'bookings.dropoff_lat',
		'dropoff_lng'			=> 'bookings.dropoff_lng',
		'pickup'				=> 'bookings.pickup',
		'dropoff'				=> 'bookings.dropoff',

		'rider'					=> 'CONCAT( riders.last_name, CONCAT(" "), riders.first_name ) rider',
		'rider_contact'			=> 'riders.contact_number rider_contact',
		'rider_photo'			=> 'riders.photo photo',

		'customer'				=> 'CONCAT( customers.last_name, CONCAT(" "), customers.first_name ) customer',
		'customer_contact'		=> 'customers.contact_number customer_contact',
		'customer_photo'		=> 'customers.photo customer_photo',
	);

	protected $aFields = array(
		'customer_id',
		'rider_id',
		'pickup_lat',
		'pickup_lng',
		'dropoff_lat',
		'dropoff_lng',
		'pickup',
		'dropoff'
	);

	function __construct(){
		$this->CI =& get_instance();
		$this->CI->load->library('db/main/Db_main_transactions');

	}

	public function get($sId = null ,$aArg = array())
	{
		$this->CI->db->select($this->select($aArg, array_merge(
			$this->aColumns,
			array(
				'customer_id' => 'bookings.customer_id'
			)
		)));

		$this->CI->db->from($this->sTable);
		$this->CI->db->join('users riders','riders.id = bookings.rider_id AND riders.role_id = 2','left');
		$this->CI->db->join('users customers','customers.id = bookings.customer_id AND customers.role_id = 3','left');

		$sId && $this->CI->db->where('bookings.customer_id = '.$sId);

		$this->search($aArg);

		$oQuery = clone $this->CI->db;
		$oQuery = $oQuery->get();

		$iRecordsFiltered = $oQuery->num_rows();
		$oQuery->free_result();

		$this->order($aArg);
		$this->limit($aArg);

		$aData 		= $this->CI->db->get();
		$aReturn 	= $aData->result_array();

		return array_merge(
			[
				'recordsFiltered' => $iRecordsFiltered,
				'recordsTotal'    => $this->CI->db->count_all($this->sTable)
			],
			array('data' => $this->getResult($aReturn,$aArg))
		);
	}

	public function add($id = null, $aArg)
	{
		$record = $this->get($id);
		if(sizeOf($record['data']) == 0) {
			$aId = $this->insert($aArg['aRequest']);
		} else {
			//move record to transaction since a driver has accepted the request
			if($record['data'][0]['rider_id'] != NULL) {

				$aArg['aRequest']['rider_id'] = $record['data'][0]['rider_id'];
				$aArg['aRequest']['pickup'] = $record['data'][0]['pickup'];
				$aArg['aRequest']['dropoff'] = $record['data'][0]['dropoff'];
				$transaction = $this->CI->db_main_transactions->add(null,$aArg);

				$record['data'][0]['id'] = $transaction['id'];
				$this->delete($id);

				return $record;
			}
		}

		$aId = $this->edit($id,$aArg);
		print_r($this->CI->db->_error_message());

		return $this->get($id);
	}

	public function edit($id, $aArg = array())
	{
		$record = null;

		if(isset($aArg['bookFromRider'])) {
			$record = $this->get($id);
			if(sizeOf($record['data']) == 0) {
				return array('iError' => 1, 'sMessage' => 'Request already expired.');
			}
		}

		$this->CI->db->where('bookings.customer_id = '.$id);
		$this->update($aArg['aRequest']);

		if(isset($aArg['bookFromRider'])) {
			$record['data'][0]['rider_id'] = $aArg['aRequest']['rider_id'];
			return $record;
		}
		return array('sMessage' => 'Record saved!', 'id' => $id);
	}

	public function delete($id)
	{
		$this->CI->db->where('bookings.customer_id = '.$id);
		$this->CI->db->delete('bookings');
	}
}
