<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_main_account extends Db_core
{
	public $CI 			= null;

	protected $sModule  = '';
	protected $sTable 	= 'users';
	protected $sIndex 	= 'id';

	protected $aColumns = array(
		'id'					=> 'users.id',

		'role_id'				=> 'users.role_id role_id',

		'username'				=> 'users.username',
		'password'				=> 'users.password',

		'fullname'				=> 'CONCAT( users.last_name, CONCAT(" "), users.first_name ) fullname'
	);

	protected $aFields = array(
		'id',
		'role_id',

		'username',
		'password'
	);

	function __construct(){
		$this->CI =& get_instance();
	}

	public function get($sId = null ,$aArg = array())
	{
		$this->CI->db->select($this->select($aArg, array_merge(
			$this->aColumns,
			array(
				'id'					=> 'users.id',
				'is_active'				=> 'users.is_active',
				'created_at'			=> 'users.created_at',
				'updated_at'			=> 'users.updated_at'
			)
		)));

		$this->CI->db->from($this->sTable);

		$this->CI->db->join('roles','roles.id = users.role_id  AND roles.is_active = 1','left');

		$this->where($aArg, 'username','users');
		$this->where($aArg, 'password','users');

        $this->CI->db->where('roles.role = "Administrator"');
		$sId && $this->CI->db->where('users.id = '.$sId);

		$this->search($aArg);

		$oQuery = clone $this->CI->db;
		$oQuery = $oQuery->get();

		$iRecordsFiltered = $oQuery->num_rows();
		$oQuery->free_result();

		$this->order($aArg);
		$this->limit($aArg);

		$aData 		= $this->CI->db->get();
		$aReturn 	= $aData->result_array();

		return array_merge(
			[
				'recordsFiltered' => $iRecordsFiltered,
				'recordsTotal'    => $this->CI->db->count_all($this->sTable)
			],
			array('data' => $this->getResult($aReturn,$aArg))
		);
	}

	public function add($id = null, $aArg)
	{
		return [
			'iErr' => 1,
			'sMessage' => 'add not allowed'
		];
	}

	public function edit($id, $aArg = array())
	{
		$aArg = $this->password($aArg);

		$aArg['aRequest']['photo'] = $this->image($aArg,$this->get($sId)['data'][0]['photo']);

		$this->CI->db->where('users.id = '.$id);
		$this->update($aArg['aRequest']);
		print_r($this->CI->db->_error_message());
		return array('sMessage' => 'Record saved!', 'id' => $id);
	}

	private function password($aRequest = [])
	{
		if(strlen($aRequest['aRequest']['password']) > 0) $aRequest['aRequest']['password'] = md5($aRequest['aRequest']['password']);
		else unset($aRequest['aRequest']['password']);

		return $aRequest;
	}

	private function image($aArg,$sOld = null) {

		if(!isset($aArg['aRequest']['photo']) || $aArg['aRequest']['photo'] == '') return null;
		if($sOld) unlink(PATH_BASE_UPLOADS.'users/'.$sOld);

		$photo = 'users/'.sha1($aArg['aRequest']['sEmail']);
		// save image
		$oImage = base64_decode(preg_replace('/^data:.*;base64,/i','',$aArg['aRequest']['photo']));

		$fOpen = fopen(PATH_BASE_UPLOADS.$photo,'w');
		if($fOpen) {
			file_put_contents(PATH_BASE_UPLOADS.$photo, $oImage);
			fclose($fOpen);

			return $photo;
		}

		return null;
	}
}
