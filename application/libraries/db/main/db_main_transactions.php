<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_main_transactions extends Db_core
{
	public $CI 			= null;

	protected $sModule  = '';
	protected $sTable 	= 'transactions';
	protected $sIndex 	= 'id';

	protected $aColumns = array(
		'id'					=> 'transactions.id',

		'customer_id'			=> 'transactions.customer_id',
		'customer'				=> 'CONCAT( customers.last_name, CONCAT(" "), customers.first_name ) customer',
		'customer_contact'		=> 'customers.contact_number customer_contact',
		'customer_photo'		=> 'customers.photo customer_photo',

		'rider_id'				=> 'transactions.rider_id',
		'rider'					=> 'CONCAT( riders.last_name, CONCAT(" "), riders.first_name ) rider',
		'rider_contact'			=> 'riders.contact_number rider_contact',
		'rider_photo'			=> 'riders.photo rider_photo',

		'pickup_lat'			=> 'transactions.pickup_lat',
		'pickup_lng'			=> 'transactions.pickup_lng',
		'dropoff_lat'			=> 'transactions.dropoff_lat',
		'dropoff_lng'			=> 'transactions.dropoff_lng',
		'status'				=> 'transactions.status',
		'pickup'				=> 'transactions.pickup',
		'dropoff' 				=> 'transactions.dropoff',
		'customer_remarks'		=> 'transactions.customer_remarks',
		'driver_remarks'		=> 'transactions.driver_remarks',
		'rate'					=> 'transactions.rate'
	);

	protected $aFields = array(
		'id',
		'customer_id',
		'rider_id',
		'pickup_lat',
		'pickup_lng',
		'dropoff_lat',
		'dropoff_lng',
		'status',
		'pickup',
		'dropoff',
		'customer_remarks',
		'driver_remarks',
		'rate'
	);

	function __construct(){
		$this->CI =& get_instance();

	}

	public function get($sId = null ,$aArg = array())
	{
		$this->CI->db->select($this->select($aArg, array_merge(
			$this->aColumns,
			array(
				'id'					=> 'transactions.id',
				'created_at'			=> 'transactions.created_at',
				'updated_at'			=> 'transactions.updated_at'
			)
		)));

		$this->CI->db->from($this->sTable);
		$sId && $this->CI->db->where('transactions.id = '.$sId);

		$this->CI->db->join('users riders','riders.id = transactions.rider_id AND riders.role_id = 2','left');
		$this->CI->db->join('users customers','customers.id = transactions.customer_id AND customers.role_id = 3','left');

		if(isset($aArg['role']) && $aArg['role'] != null) {
			$this->CI->db->where( 'transactions.'. ($aArg['role'] == 'Rider' ? 'rider_id' : 'customer_id') . ' = '.$aArg['id']);
			$this->CI->db->order_by('transactions.id DESC');
			$this->CI->db->limit('1');
		}

		if(isset($aArg['rider_id']) && $aArg['rider_id'] != null) {
			$this->CI->db->where('transactions.rider_id = '.$aArg['rider_id']);
			$this->CI->db->where('transactions.customer_id = "'.$aArg['customer_id'].'"');
			$this->CI->db->order_by('transactions.id DESC');
			$this->CI->db->limit('1');
		}

		$this->search($aArg);

		$oQuery = clone $this->CI->db;
		$oQuery = $oQuery->get();
		// print_r($this->CI->db->_error_message());

		$iRecordsFiltered = $oQuery->num_rows();
		$oQuery->free_result();

		if(!isset($aArg['rider_id']) || (isset($aArg['rider_id']) && $aArg['rider_id'] == null)) {
			$this->order($aArg);
			$this->limit($aArg);
		}

		$aData 		= $this->CI->db->get();
		$aReturn 	= $aData->result_array();

		// print_r($this->CI->db->last_query());

		return array_merge(
			[
				'recordsFiltered' => $iRecordsFiltered,
				'recordsTotal'    => $this->CI->db->count_all($this->sTable)
			],
			array('data' => $this->getResult($aReturn,$aArg))
		);
	}

	public function add($id = null, $aArg)
	{
		$aId = $this->insert($aArg['aRequest']);
		print_r($this->CI->db->_error_message());

		return array('sMessage' => 'Record saved!', 'id' => $aId['id']);
	}

	public function edit($id, $aArg = array())
	{
		$this->CI->db->where('transactions.id = '.$id);
		$this->update($aArg['aRequest']);

		print_r($this->CI->db->_error_message());
		return array('sMessage' => 'Record saved!', 'id' => $id);
	}
}
