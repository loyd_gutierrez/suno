<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_main_settings extends Db_core
{
	public $CI 			= null;

	protected $sModule  = '';
	protected $sTable 	= 'settings';
	protected $sIndex 	= 'id';

	protected $aColumns = array(
		'perKM'				=> 'settings.perKM',
		'minimumRate'		=> 'settings.minimumRate'
	);

	protected $aFields = array(
		'perKM',
		'minimumRate',
		'username',
		'password'
	);

	function __construct(){
		$this->CI =& get_instance();

		$this->CI->load->library('db/main/db_main_users');
	}

	public function get($sId = null ,$aArg = array())
	{
		$this->CI->db->select($this->select($aArg, array_merge(
			$this->aColumns,
			array(
				'perKM'					=> 'settings.perKM',
				'minimumRate'			=> 'settings.minimumRate'
			)
		)));

		$this->CI->db->from($this->sTable);

		$oQuery = clone $this->CI->db;
		$oQuery = $oQuery->get();

		$iRecordsFiltered = $oQuery->num_rows();
		$oQuery->free_result();

		$this->order($aArg);
		$this->limit($aArg);

		$aData 		= $this->CI->db->get();
		$aReturn 	= $aData->result_array();

		$admin = $this->CI->db_main_users->get(1);

		$data = $this->getResult($aReturn,$aArg);
		$data[0]['username'] = $admin['data'][0]['username'];

		return array('data' => $data );
	}

	public function add($id = null, $aArg)
	{
		return [
			'iErr' => 1,
			'sMessage' => 'add not allowed'
		];
	}

	public function edit($id, $aArg = array())
	{
		$this->CI->db_main_users->edit(1,[
			'aRequest' => [
				'username' => $aArg['aRequest']['username'],
				'password' => $aArg['aRequest']['password']
			]
		]);

		$this->update([
			'minimumRate' => $aArg['aRequest']['minimumRate'],
			'perKM'	=> $aArg['aRequest']['perKM']
		]);

		print_r($this->CI->db->_error_message());
		return array('sMessage' => 'Record saved!', 'id' => $id);
	}

	private function password($aRequest = [])
	{
		if(strlen($aRequest['aRequest']['password']) > 0) $aRequest['aRequest']['password'] = md5($aRequest['aRequest']['password']);
		else unset($aRequest['aRequest']['password']);

		return $aRequest;
	}

	private function image($aArg,$sOld = null) {

		if(!isset($aArg['aRequest']['photo']) || $aArg['aRequest']['photo'] == '') return null;
		if($sOld) unlink(PATH_BASE_UPLOADS.'settings/'.$sOld);

		$photo = 'settings/'.sha1($aArg['aRequest']['sEmail']);
		// save image
		$oImage = base64_decode(preg_replace('/^data:.*;base64,/i','',$aArg['aRequest']['photo']));

		$fOpen = fopen(PATH_BASE_UPLOADS.$photo,'w');
		if($fOpen) {
			file_put_contents(PATH_BASE_UPLOADS.$photo, $oImage);
			fclose($fOpen);

			return $photo;
		}

		return null;
	}
}
