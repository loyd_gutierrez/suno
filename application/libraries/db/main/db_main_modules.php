<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_main_modules extends Db_core
{

	public $CI 			= null;

	function __construct(){
		$this->CI =& get_instance();
	}

	public function get($sId = null,$aArg = array())
	{
		$aUserInfo = $this->CI->session->userdata('aInfo');

		switch($aUserInfo['role']){
			case "Administrator":
				return array(
					'id' => $aUserInfo['id'],
					'role' => $aUserInfo['role'],
					'fullname' => $aUserInfo['fullname'],
					'photo' 	=> $aUserInfo['photo'],
					'sLandingPage' => 'Settings',
					'aModules' => array(
						// array( 'sName' => 'Account', 'sTitle' => 'Account','bNavigation'=>true,'bList' => true, 'bForm' => true, 'sIcon' => 'fa-lock', 'aPermissions' => array('menu','edit') ),
						array( 'sName' => 'Riders', 'sTitle' => 'Riders','bNavigation'=>true,'bList' => true, 'bForm' => true, 'sIcon' => 'fa-motorcycle', 'aPermissions' => array('menu','add','edit','delete') ),
						array( 'sName' => 'Customers', 'sTitle' => 'Customers','bNavigation'=>true,'bList' => true, 'bForm' => true, 'sIcon' => 'fa-user', 'aPermissions' => array('menu','add','edit','delete') ),
						array( 'sName' => 'Settings', 'sTitle' => 'Settings','bNavigation'=>true,'bList' => true, 'bForm' => true, 'sIcon' => 'fa-gears', 'aPermissions' => array('menu','add','edit','delete') ),
						array( 'sName' => 'Transactions', 'sTitle' => 'Transactions','bNavigation'=>true,'bList' => true, 'bForm' => true, 'sIcon' => 'fa-file', 'aPermissions' => array('menu','edit','delete') )
						// array( 'sName' => 'Managements', 'sTitle' => 'Management','bNavigation'=>true,'bList' => false, 'bForm' => false, 'sIcon' => 'fa-gears', 'aPermissions' => array('menu'), 'aChildren' => array(

						// 	)
						// )
					)
				);
			break;

			default:
				return array('iErr' => 1, 'sMessage' => 'Permission Denied! Role not found');
			break;
		}

	}
}
