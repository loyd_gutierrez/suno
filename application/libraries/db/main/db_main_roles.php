<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_main_roles extends Db_core
{

	public $CI 			= null;

	protected $sModule  = '';
	protected $sTable 	= 'roles';
	protected $sIndex 	= 'id';

	protected $aColumns = array(
		'role'					=> 'roles.role',
		'is_active'				=> 'roles.is_active'
	);

	protected $aFields = array(

		'tole',
		'is_active'

	);

	function __construct(){
		$this->CI =& get_instance();
	}

	public function get($sId = null ,$aArg = array())
	{
		$this->CI->db->select($this->select($aArg, array_merge(
			$this->aColumns,
			array(
				'is_active'				=> 'roles.is_active',
				'id'				=> 'roles.id',
			)
		)));

		$this->CI->db->from($this->sTable);
		$this->where($aArg, 'id','roles');
		$this->where($aArg, 'role','roles');

		$this->search($aArg);

		$oQuery = clone $this->CI->db;
		$oQuery = $oQuery->get();
		$iRecordsFiltered = $oQuery->num_rows();
		$oQuery->free_result();

		$this->order($aArg);
		$this->limit($aArg);

		$aData 		= $this->CI->db->get();
		$aReturn 	= $aData->result_array();

		return array_merge(
			[
				'recordsFiltered' => $iRecordsFiltered,
				'recordsTotal'    => $this->CI->db->count_all($this->sTable)
			],
			array('data' => $this->getResult($aReturn,$aArg))
		);
	}

	public function add($aArg)
	{

	}

	public function edit($aArg = array())
	{

	}
}
