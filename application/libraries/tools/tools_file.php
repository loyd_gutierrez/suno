<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @name        File Handler Library
 * @author      Elowie Cruz <elowie.cruz@gmail.com>
 * @version		1.0.0
 */


class Tools_file
{
	protected $CI		= NULL;
	protected $sTemp	= NULL;
	protected $iExpiry	= 86400;

	public function __construct($aArg = array())
	{
		$this->CI	=& get_instance();
		$this->CI->load->helper('file');
	}

	public function directory($aArg = array())
	{
		if (!is_dir($aArg['sPath'])) {
			$aArg['sPath']	= rtrim($aArg['sPath'], "/");
			$aResp			= $this->directory(array(
				'sPath'	=> substr($aArg['sPath'], 0, strrpos($aArg['sPath'], '/')) 
			));
			if ($aResp['iErr'] || !mkdir($aArg['sPath'], 0755, TRUE)) return array(
				'iErr'	=> 1,
			);
		}

		return array(
			'iErr'	=> 0
		);
	}

    public function delete($sPath)
    {
    	return is_file($sPath) === TRUE && @unlink($sPath);
    }

	public function name($aArg = array())
	{
		$aNames	= $this->names($aArg);
		while (isset($aNames[$sName = $this->random()]));
		return $sName;
	}

	public function read($aArg)
	{
		if (file_exists($aArg['sFile'])) {
			$sData	= fread(($rFile = fopen($aArg['sFile'], "r")), filesize($aArg['sFile']));
			fclose($rFile);
			return array(
				'iErr'	=> 0,
				'aData'	=> array(
					'sMime'	=> $this->mime($aArg['sFile']),
					'sData'	=> $sData
				)
			);
		} else {
			return array(
				'iErr'	=> 4002,
				'sMsg'	=> 'Invalid File' 
			);
		}
	}

	public function base64($aArg = array())
	{
		if (($aResp = $this->read($aArg)) && $aResp['iErr']) return $aResp;
		return 'data:'.$aResp['aData']['sMime'].';base64,'.base64_encode($aResp['aData']['sData']);
	}

	public function cleanup($aArg = array())
	{
		$aFiles	= $this->files($aArg);
		foreach ($aFiles as $aFile) {
			(time() - $aFile['date']) > 86400 && $this->delete($aFile['server_path']);	
		}
	}

	public function mime($sFile)
	{
		$rInfo	= finfo_open(FILEINFO_MIME_TYPE);
		$sMime	= finfo_file($rInfo, $sFile);
		finfo_close($rInfo);
		return $sMime;
	}

	public function random()
	{
		list($usec, $sec) = explode(" ", microtime());
		return sha1(((float)$usec + (float)$sec) * rand() * rand());
	}

	protected function names($aArg = array())
	{
		$aNames	= array();
		$aFiles	= $this->files($aArg);
		
		if ($aFiles) {
			foreach ($aFiles as $sName => $aFile) {
				$aTempName	= explode('.', $sName);
				$aNames[]	= array_shift($aTempName);
			}
		}
		return $aNames;
	}

	protected function files($aArg = array())
	{
		return get_dir_file_info(
			isset($aArg['sTemp']) ?
			$aArg['sTemp'] :
			$this->sTemp
		);
	}
}



