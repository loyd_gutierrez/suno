<?php defined('BASEPATH') OR exit('No direct script access allowed');

class DT extends CI_Model
{
	function __construct()
    {
        parent::__construct();
        header('content-type:json');
    }

    public function get($sModule,$aRequest)
    {
        $sModule = 'db_main_'.$sModule;

    	if(!class_exists($sModule)) $this->load->library('db/main/'.$sModule.'.php');

    	$aParams   = array(
            'aColumns'  => $this->getColumns($aRequest),
            'aOrder'    => $this->getOrder($aRequest),
            'iLimit'    => $this->getLimit($aRequest),
            'iOffset'   => $this->getOffset($aRequest),
            'sSearch'   => $aRequest['aRequest']['search']['value']
        );
        
    	$aData     = $this->$sModule->get(null,array_merge(
            $aParams,
            $this->getFilters($aRequest)
        ));

        return array(
            'draw'              => $aRequest['aRequest']['draw'] + 1,
            'recordsTotal'      => $aData['recordsTotal'],
            'recordsFiltered'   => $aData['recordsFiltered'],
            'data'              => $aData['data']
        );

        
    }

    private function getFilters($aArg = array())
    {
        $aFilters   = array();
        foreach ($aArg['aRequest']['columns'] as $aVal) {
            $aVal['data'] && strlen($aVal['search']['value']) && $aVal['search']['value'] != 'null' && (
                $aFilters[$aVal['data']] = $aVal['search']['value']
            );
        }
        return $aFilters;
    }

    private function getLimit($aArg = array())
    {
        return (
            isset($aArg['aRequest']['length']) && $aArg['aRequest']['length'] > 0 ?
            $aArg['aRequest']['length'] :
            NULL
        );
    }

    private function getOffset($aArg = array())
    {
        return (
            isset($aArg['aRequest']['start']) ?
            $aArg['aRequest']['start'] :
            NULL
        );
    }

    private function getOrder($aArg = array())
    {
        $aOrder     = array();
        $aColumns   = $aArg['aRequest']['columns'];

        foreach ($aArg['aRequest']['order'] as $aVal) {
            $aOrder[] = [$aColumns[$aVal['column']]['data'],strtoupper($aVal['dir'])];
        } 
        return $aOrder;
    }

    private function getColumns($aArg = array())
    {
        $aColumns   = array();
        foreach ($aArg['aRequest']['columns'] as $aVal) {
            isset($aVal['data']) && ($aColumns[] = $aVal['data']);
        }
        return $aColumns;
    }
}