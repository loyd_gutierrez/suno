<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mpdf extends CI_Model
{
    function __construct()
    {
        parent::__construct();

        $this->load->library('tools/tools_mpdf', array(
			'mode'	=> 'c'
		));
    }

	public function print_certificate($sModule, $aArg = array())
	{
		$pdfFile = FCPATH.'assets/pdf/certificate.pdf';
		if (file_exists(FCPATH.'assets/css/pdf/reports/libs/'.$sModule.'.css')) {
			$this->tools_mpdf->WriteHTML(file_get_contents(FCPATH.'assets/css/pdf/reports/libs/'.$sModule.'.css'), 1);
		}

		$this->tools_mpdf->SetTitle('Vessel Certificates');
		$this->tools_mpdf->WriteHTML(file_get_contents(FCPATH.'assets/css/pdf/reports/core.css'), 1);
		$this->tools_mpdf->WriteHTML($this->load->view('certificates/'.$sModule, $aArg, TRUE));
		$this->tools_mpdf->Output();
	}
}