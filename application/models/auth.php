<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends Api
{

	function __construct()
	{
		parent::__construct();
		$this->load->library('users.php');
	}

	function info(){
		return array(
			'id'   			=> $this->tools_sessions->get('id'),
			'username'  	=> $this->tools_sessions->get('username'),
			'fullname'  	=> $this->tools_sessions->get('fullname'),
			'role'  		=> $this->tools_sessions->get('role'),
			'photo'  		=> $this->tools_sessions->get('photo'),
		);
	}

	function login($aArg = array())
	{
		//not woriing! codes in login is in rest.php
	}

	function logout()
	{
		$this->tools_sessions->logout();
		return array(
				'sMessage' => 'success'
			);
	}
}
