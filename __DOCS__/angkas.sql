-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 05, 2017 at 01:54 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `angkas`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `rider_id` int(10) UNSIGNED DEFAULT NULL,
  `pickup_lat` float NOT NULL,
  `pickup_lng` float NOT NULL,
  `dropoff_lat` float NOT NULL,
  `dropoff_lng` float NOT NULL,
  `pickup` text COLLATE utf8_unicode_ci,
  `dropoff` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` int(11) NOT NULL,
  `perKM` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `riderscoords`
--

CREATE TABLE `riderscoords` (
  `id` int(10) UNSIGNED NOT NULL,
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `role` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` int(1) UNSIGNED NOT NULL,
  `updated_at` int(1) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 1, 1506325326, NULL),
(2, 'Rider', 1, 1506325326, NULL),
(3, 'Customer', 1, 1506325339, NULL);

--
-- Triggers `roles`
--
DELIMITER $$
CREATE TRIGGER `roles_BINS` BEFORE INSERT ON `roles` FOR EACH ROW BEGIN
	set new.created_at = unix_timestamp();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `roles_BUPD` BEFORE UPDATE ON `roles` FOR EACH ROW BEGIN
	set new.updated_at = unix_timestamp();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `perKM` float UNSIGNED DEFAULT NULL,
  `minimumRate` float UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`perKM`, `minimumRate`) VALUES
(10, 42);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(1) UNSIGNED NOT NULL,
  `rider_id` int(1) UNSIGNED NOT NULL,
  `customer_id` int(1) UNSIGNED NOT NULL,
  `pickup_lat` float NOT NULL,
  `pickup_lng` float NOT NULL,
  `dropoff_lat` float NOT NULL,
  `dropoff_lng` float NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 - success',
  `pickup` text COLLATE utf8_unicode_ci,
  `dropoff` text COLLATE utf8_unicode_ci,
  `customer_remarks` text COLLATE utf8_unicode_ci,
  `driver_remarks` text COLLATE utf8_unicode_ci,
  `rate` float UNSIGNED NOT NULL,
  `created_at` int(1) UNSIGNED NOT NULL,
  `updated_at` int(1) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `transactions`
--
DELIMITER $$
CREATE TRIGGER `transactions_BINS` BEFORE INSERT ON `transactions` FOR EACH ROW BEGIN
	set new.created_at = unix_timestamp();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `transactions_BUPD` BEFORE UPDATE ON `transactions` FOR EACH ROW BEGIN
	set new.updated_at = unix_timestamp();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(1) UNSIGNED NOT NULL,
  `role_id` int(1) UNSIGNED NOT NULL,
  `is_active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `birthdate` int(1) UNSIGNED DEFAULT NULL,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `middle_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `contact_number` varchar(13) COLLATE utf8_unicode_ci NOT NULL,
  `or_cr` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plate_number` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `license_number` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo_motor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(1) UNSIGNED NOT NULL,
  `updated_at` int(1) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `is_active`, `birthdate`, `username`, `password`, `last_name`, `first_name`, `middle_name`, `photo`, `gender`, `contact_number`, `or_cr`, `plate_number`, `license_number`, `photo_motor`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, 'admin', 'e99a18c428cb38d5f260853678922e03', 'Doe', 'John', NULL, NULL, 'm', '09167217398', NULL, NULL, NULL, NULL, 1512235034, 1507373133),
(3, 2, 1, 1970, 'rider', 'e99a18c428cb38d5f260853678922e03', 'Rider123', 'Name', 'A', NULL, 'm', '7561234', NULL, NULL, NULL, NULL, 1512235034, 1510048718),
(11, 3, 1, 1970, 'customer', 'e99a18c428cb38d5f260853678922e03', 'PEDRO', 'A', 'L', 'users/17ba0791499db908433b80f37c5fbc89b870084b', 'm', '09167217398', NULL, NULL, NULL, NULL, 1512235034, 1512478412),
(12, 2, 1, 1981, 'rider2', 'e99a18c428cb38d5f260853678922e03', 'Rider 2', 'Mask', 'A', NULL, 'm', '09172221111', NULL, NULL, NULL, NULL, 1512235034, 1510045090),
(13, 3, 1, 1970, 'customer2', 'e99a18c428cb38d5f260853678922e03', 'Lastname', 'firstname', 'middlename', 'users/bd307a3ec329e10a2cff8fb87480823da114f8f4', 'm', '09167217398', NULL, NULL, NULL, NULL, 1512235034, 1512478466),
(15, 2, 1, 1970, 'asdf', 'e99a18c428cb38d5f260853678922e03', 'gwapito', 'rider', 'na', 'users/f1abd670358e036c31296e66b3b66c382ac00812', 'm', '09168218312', NULL, NULL, NULL, NULL, 1512235034, 1512478477);

--
-- Triggers `users`
--
DELIMITER $$
CREATE TRIGGER `users_BINS` BEFORE INSERT ON `users` FOR EACH ROW BEGIN
		
	SET NEW.created_at = unix_timestamp();

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `users_BUPD` BEFORE UPDATE ON `users` FOR EACH ROW BEGIN
	set new.updated_at = unix_timestamp();
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bookings.rider_id_idx` (`rider_id`),
  ADD KEY `bookings.customer_id_idx` (`customer_id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `riderscoords`
--
ALTER TABLE `riderscoords`
  ADD KEY `riders.id_idx` (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions.driver_id_idx` (`rider_id`),
  ADD KEY `transactions.customer_id_idx` (`customer_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users.role_id_idx` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(1) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `bookings`
--
ALTER TABLE `bookings`
  ADD CONSTRAINT `bookings.customer_id` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `bookings.rider_id` FOREIGN KEY (`rider_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `riderscoords`
--
ALTER TABLE `riderscoords`
  ADD CONSTRAINT `riders.id` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions.customer_id` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `transactions.driver_id` FOREIGN KEY (`rider_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users.role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
