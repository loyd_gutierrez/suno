$(document).ready(function(){
	app.init();
});

window.app = {
	init : function(){

		var oSelf = this;

		HTTP.request('get','modules',null,function(aResp){

			if(aResp.iError) {
				if(aResp.iError == 401) app.login.init()
				else toastr['error']( aResp.sMessage ,'You have an Error')
			} else {

				$('div.view').css('display','none');
				$('div.view[name="app"]').css('display','block');

				$('a[name="btn_logout"]').unbind().click(oSelf.aActions.logout);
				$('a[name="btn_editProfile"]').unbind().click(function(){
					if(!oModules.Users.aWidgets.Users.bInit) oModules.Users.aWidgets.init({});
					oModules.Users.aWidgets.Users.forms({ sId : aResp[0]['aItem'].id });
				});
				oSelf.navigation.init(aResp);
			}
		});
	},

	login : {
		init : function(){
			$('div.view').css('display','none');
			$('div.view[name="login"]').css('display','block');

			var oForm = $('body div[name="login"] form');

			oForm.validate({
				rules : {
					username : {required:true,maxlength:32},
					password : {required:true,maxlength:32}
				}
			});

			oForm.submit(function(){
				if(!$(oForm).valid()) return toastr['error']('Please input your email and password','Login Error');

				var aRequest = {
					username : $(oForm.get(0).username).val(),
					password : $(oForm.get(0).password).val(),
				};

				$(oForm.get(0).password).val("");

				HTTP.request('POST','auth',aRequest,function(aResp){
					return aResp.iError ?
						toastr['error']( aResp.sMessage ,'You have an Error') :
						app.init();
				});
			});
		}
	},

	navigation : {
		sidebar : null,
		aPermissions : {},
		init : function(aData){

			var oSelf = this;
			var aUser = aData[0]['aItem'];
			oSelf.sidebar = $('aside.main-sidebar section.sidebar ul.sidebar-menu');

			oData.user_id = aUser.id;
			(oData.fullname = aUser.fullname) && $('#sUserName').text(aUser.fullname);
			aUser.photo && $('.user-panel img.profile-user-img').attr('src', 'assets/files/uploads/'+aUser.photo+'?rand='+ new Date().getTime());

			this.generate(aData.aContents,function(){
				oModules.load({ sModule : aUser.sLandingPage });
			});

			oSelf.sidebar.find('li a').unbind().click(function(){
				oModules.load({ sModule : $(this).attr('name') });
			});
		},

		generate : function(aData,fCb){
			var oSelf = this;
			var sTemp = "";

			$.each( aData.aModules,function(iKey,aMenu){

				oSelf.aPermissions[ aMenu.sName ] = aMenu.aPermissions;

				if(aMenu.bNavigation) {
					var sLink = '<a name="'+ aMenu.sName +'" href="javascript:void(0)"><i class="fa '+ aMenu.sIcon +'"></i><span>'+ aMenu.sTitle +'</span>'+ (aMenu.aChildren ? '<i class="fa fa-angle-left pull-right"></i>' : '') +'</a>';
					sTemp+= '<li class="treeview">'+ sLink;

					if(aMenu.aChildren) {
						sTemp+= '<ul class="treeview-menu">';
						$.each(aMenu.aChildren,function(iKey,aSubmenu){

							oSelf.aPermissions[ aSubmenu.sName ] = aSubmenu.aPermissions;
							sTemp+=  '<li class="treeview"><a  name="'+ aSubmenu.sName +'" href="javascript:void(0)"><i class="fa '+ aSubmenu.sIcon +'"></i><span> '+ aSubmenu.sTitle +' </span></a></li>';
							oSelf.ModuleContents(aSubmenu,aData);
						});
						sTemp+='</ul>';
					}
					sTemp+= '</li>';

				}
				oSelf.ModuleContents(aMenu,aData);
			});

			oSelf.sidebar.append(sTemp);
			fCb && fCb();
		},

		ModuleContents : function(aRow,aArg){
			aArg.lists && aArg.lists[ aRow.sName ] && $('.module-content .lists').append( aArg.lists[ aRow.sName ] );
			aArg.forms && aArg.forms[ aRow.sName ] && $('.module-content .forms').append( aArg.forms[ aRow.sName ] );
			aArg.js && aArg.js[ aRow.sName ] && eval( aArg.js[ aRow.sName ] );

			if( aRow.aChildren ) this.ModuleContents(aRow.aChildren,aArg);
		}
	},

	aActions : {
		download : function(oButton,sPath,sId){
			window.open('archives/'+sId);
		},
		logout : function(){
			var oForm = $('div[name="Confirm"][role="dialog"]');

			oForm.find('.modal-body').empty().append('Are you sure you want to signout your account?');

			oForm.find('button[name="Confirm"]').unbind().click( function(){
				HTTP.request('POST','logout',{},function(aResp){
					if(aResp.iError ){
						toastr['error']( aResp.sMessage ,'You have an Error')
					}
					location.reload();
				});
			});

			oForm.modal({
				keyboard: false,
				backdrop: 'static'
			});
		},
		save : function(oWidget,fCb){

			var aRequest = oForms.get(oWidget);

			var aArg = {
				sModule : oWidget.sModuleName,
				aRequest: aRequest
			};

			aArg[ oWidget.aDatatable.sIndex ] = $(oWidget.oForm[ oWidget.aDatatable.sIndex ]).val();

			sReqType = aArg[ oWidget.aDatatable.sIndex ] ? 'PUT' : 'POST';
			sModule = oWidget.sModuleName + ( aArg[ oWidget.aDatatable.sIndex ] ? '/'+aArg[ oWidget.aDatatable.sIndex ] : '');

			HTTP.request(sReqType,(sModule).toLowerCase(),aArg,function(aResp){
				if(aResp.iError > 0){
					return toastr['error']( aResp.sMessage ,'You have an Error');
				}

				toastr['success'](aResp ? aResp.sMessage : 'Record successfully saved',oWidget.sModuleName);
				oTables.reload(oWidget);
				oWidget.oModal.modal('hide');

				fCb && fCb(aResp);
			});
		},

		delete : function(oNode,sPath,sId,bDelete){

			var aPath = sPath.split('/');
			var oWidget = oModules[aPath[0]].aWidgets[ aPath[1] ];
			var oForm = $('div[name="Confirm"][role="dialog"]');

			var aArg = {
				sModule	: oWidget.sModuleName,
				aRequest : {
					is_active : 0
				}
			};
			aArg[ oWidget.aDatatable.sIndex ] = sId;

			oModals.confirm(oNode,oForm,function(){

				HTTP.request('PUT', (oWidget.sModuleName).toLowerCase() +'/'+sId, aArg, function(aResp){

					if(aResp.iError > 0){
						return toastr['error']( aResp.sMessage ,'You have an Error');
					}

					toastr['success'](aResp ? aResp.sMessage : 'Record successfully deleted',oWidget.sModuleName);
					oForm.modal('hide');
					oTables.reload(oWidget);
				});

			});
		},

		revive : function(oNode,sPath,sId,bDelete){

			var aPath = sPath.split('/');
			var oWidget = oModules[aPath[0]].aWidgets[ aPath[1] ];
			var oForm = $('div[name="Confirm"][role="dialog"]');

			var aArg = {
				sModule	: oWidget.sModuleName,
				aRequest : {
					is_active : 1
				}
			};
			aArg[ oWidget.aDatatable.sIndex ] = sId;

			oModals.confirm(oNode,oForm,function(){

				HTTP.request('PUT', (oWidget.sModuleName).toLowerCase() +'/'+sId, aArg, function(aResp){

					if(aResp.iError > 0){
						return toastr['error']( aResp.sMessage ,'You have an Error');
					}

					toastr['success'](aResp ? aResp.sMessage : 'Record successfully deleted',oWidget.sModuleName);
					oForm.modal('hide');
					oTables.reload(oWidget);
				});

			});
		}
	}
};

/**
 * Handles datatables view
 */
window.oTables =
{
	sTableName : null,

	init: function(oWidget,aArg)
	{
		var bState = false;
		var aPermissions = app.navigation.aPermissions[ oWidget.sModuleName ];

		oWidget.oList = $(oWidget.sList).get(0);
		aArg.aCustom && $(oWidget.aDatatable.sName).data('aCustom',aArg.aCustom);

		this.sTableName = oWidget.aDatatable.sName;

		((aPermissions.indexOf('add') > -1) || (aPermissions.indexOf('edit') > -1) || (aPermissions.indexOf('delete') > -1) || (aPermissions.indexOf('view') > -1)) && oWidget.aDatatable.aColumns.push({
			bSortable : false,
			mData: null,
			sWidth: 50,
			sTitle: (
					(aPermissions.indexOf('add') > -1) ?
					'<div class="btn-group col-sm-12">'+
						'<button action="add" type="button" onclick=oTables.action(this,"'+oWidget.sPath+'"); class="btn btn-default btn-sm fa">&#xf067;</button>'+
					'</div>' : ''
				),
			render: function(data,type,row){
				var aButtons = [];

				(aPermissions.indexOf('delete') > -1) && aButtons.push({
					sIcon : row.is_active == 1 ? 'fa fa-trash' : 'fa fa-refresh',
					sAction : row.is_active == 1 ? 'delete' : 'revive',
					sText : row.is_active == 1 ? 'Delete' : 'revive'
				});

				(aPermissions.indexOf('edit') > -1) && aButtons.push({
					sIcon : 'fa fa-pencil-square-o',
					sAction : 'edit',
					sText : 'Edit'
				});

				(aPermissions.indexOf('view') > -1) && aButtons.push({
					sIcon : 'fa fa-search',
					sAction : 'View',
					sText : 'View'
				});

				(aPermissions.indexOf('download') > -1) && aButtons.push({
					sIcon : 'fa fa-download',
					sAction : 'download',
					sText : 'Download'
				});

				var sMenu = '';

				for(i = 0; i < aButtons.length ; i++) {
					(aButtons.length > 2)  && (sMenu+= '<li><a href="javascript:void(0)" action="'+ aButtons[i].sAction +'"><i class="'+ aButtons[i].sIcon +'"></i>'+ aButtons[i].sText +'</a></li>');
					(aButtons.length < 3) && (sMenu+= '<button action="'+ aButtons[i].sAction +'" type = "button" class="btn btn-default btn-sm"><i class="'+ aButtons[i].sIcon +'"></i></button>');
				}

				return '<div class="btn-group">'+(
					(aButtons.length > 2) ?
						'<button class="btn dropdown-toggle btn-default btn-sm fa fa-bars" data-toggle="dropdown" aria-expanded="false"></button>'+
							'<ul class="dropdown-menu pull-right">'+ sMenu +'</ul>' : sMenu
				) +'</div>';

			},
			createdCell	: function(sButtons, cellData, rowData, row, col)
			{
				$(sButtons).find('[action]').unbind().click(function(){
					oTables.action(this, oWidget.sPath, rowData[ oWidget.aDatatable.sIndex ] );
				});
			}

		});

		var oSelf = this;
		var oTable = $('table[name="'+ oTables.sTableName +'"]');

		oWidget.oTable = oTable.DataTable({
			serverSide : true,
			oLanguage : {
				oPaginate	: {
					sPrevious	: '<i class="ion-chevron-left"></i>',
					sNext		: '<i class="ion-chevron-right"></i>'
				}
			},
			paging: true,
			processing: true,
			bInfo: true,
			columns : oWidget.aDatatable.aColumns,
			order : (oWidget.aDatatable.aOrder ? oWidget.aDatatable.aOrder : []),

			ajax : function(aRequest, fCallback, aSettings)
			{
				var aColumns = oTables.aRequest( aRequest.columns );

				aRequest.custom = (
					$(this.selector).data('aCustom') || null
				);

				HTTP.request('get',('tables/'+oWidget.sModuleName).toLowerCase(),{
					sType: 'datatables',
					aRequest : aRequest
				},function(aResp){
					fCallback(aResp);
				});
			}
		});
	},

	reload : function(oWidget)
	{
		if(!$('table[name="'+ oWidget.aDatatable.sName +'"]')) return

		($(oWidget.sList).length > 0) && oWidget.aDatatable && oWidget.aDatatable.sName && $('table[name="'+ oWidget.aDatatable.sName +'"]').dataTable()._fnAjaxUpdate();
	},
	aRequest : function(aArg)
	{
		var aColumns = [];
		$.each(aArg, function(iKey,aRow){
			aColumns.push(aRow.data);
		});

		return aColumns;
	},

	aOrder : function(aColumns,aArg)
	{
		var aOrder = [];
		$.each(aArg,function(iKey,aRow){
			aOrder.push(aColumns[aRow.column]+' '+aRow.dir);
		});

		return aOrder;
	},

	filter : function(oWidget, iIndex, mVal)
	{
		window.test = $(oWidget.aDatatable.sName);
		$(oWidget.aDatatable.sName).dataTable().api().columns(iIndex).search(mVal !== undefined ? mVal : '').draw();
	},

	/**
	 * Manages all actions from datatables
	 * @param  {object} oButton : Button that is responsible for the command.
	 * @param  {string} sPath   : Module location
	 * @return {[type]}         [description]
	 */
	action: function(oButton,sPath,sId)
	{
		if(app.aActions[ $(oButton).attr('action') ]) return app.aActions[ $(oButton).attr('action') ](oButton,sPath,sId);

		var aPath = sPath.split('/');
		var oWidget = oModules[aPath[0]].aWidgets[ aPath[1] ];

		$(oWidget.sForm).find('form').attr('method', $(oButton).attr('action') );

		if($(oButton).attr('action') == 'View') {
			$(oWidget.sForm).find('.modal-footer button[action="save"]').css('display','none');
		} else {
			$(oWidget.sForm).find('.modal-footer button[action="save"]').css('display','inline-block');
		}

		oWidget.forms({ sId: sId });
	}
}

/**
 * Handles popup modals
 */
window.oForms = {
	init : function(oWidget){

		oModals.init(oWidget);
		var oSelf = this;

		oWidget.aValidation && $.each(oWidget.aValidation.rules,function(sField){
			sFieldType = $(oWidget.oForm[ sField ]).attr('field-type') || 'Default';
			oSelf[ sFieldType ].init && oSelf[ sFieldType ].init( oWidget, $(oWidget.oForm[ sField ]) );

		});

		oWidget.bInit = true;

	},
	/**
	 * Set value of each fields
	 * @param {[type]} oWidget   Module]
	 * @param {[type]} aRequests Json Request
	 * @param {[type]} aItems    Json Result
	 */
	set : function(oWidget,aArg,aRequests,aRecords){
		var oSelf = this;

		oWidget.aValidation && $.each(oWidget.aValidation.rules,function(oField){
			if( aRequests[1].indexOf(oField) > -1) {
				oSelf[ $(oWidget.oForm[ oField ]).attr('field-type') ].populate( $(oWidget.oForm[ oField ]), aRecords[ aRequests[1].indexOf(oField) ].data );
			}

			aArg.sId && $(oWidget.oForm[ oField ]) && oSelf[ $(oWidget.oForm[ oField ]).attr('field-type') || 'Default' ].set( $(oWidget.oForm[ oField ]), aRequests, aRecords );
		});
	},
	get : function(oWidget)
	{
		var oSelf = this;
		var aRequest = {};
		oWidget.aValidation && $.each(oWidget.aValidation.rules, function(sKey){
			(sKey != oWidget.aDatatable.sIndex) && (aRequest[sKey] = oSelf[ $(oWidget.oForm[ sKey ]).attr('field-type') || 'Default' ].get( $(oWidget.oForm[sKey]), oWidget) );
		});
		return aRequest;
	},
	reset : function(oWidget){


		var oSelf = this;
		oWidget.aValidation && $.each(oWidget.aValidation.rules,function(sField){
			if(['iRegistrationTypeId','iVesselCategoryId','iVesselTypeId'].indexOf(sField) > -1) {
				return $(oWidget.oForm[ sField ]).filter(':checked').prop('checked',false);
			}
			$(oWidget.oForm[ sField ]) && oSelf[ $(oWidget.oForm[ sField ]).attr('field-type') || 'Default' ].reset(oWidget,$(oWidget.oForm[sField]));
		});

		$(oWidget.oForm).validate().resetForm();
	},
	File : {
		init : function(oWidget, oField){
			$(oField).change(function(){
				if(event.target.files && event.target.files[0]) {
					var target = event.target.files;
					var reader = new FileReader();

					$(oWidget.oForm.filesize).val( target[0].size );
					$(oWidget.oForm.filetype).val( target[0].type );
					$(oWidget.oForm.fileext).val( target[0].name.split(".").pop() );

					reader.onload = function (e) {
						oWidget.file_data = e.target.result;
					}
					reader.readAsDataURL(event.target.files[0]);
				}
			});
		},
		set: function(oField,aRequests,aRecords) {
		},
		get: function(oField,oWidget) {
			return oWidget.file_data;
		},
		reset : function(oWidget,oField) {
			return $(oField).attr('src', $(oField).attr('field-default') || '' );
		}
	},
	/** Upload photo */
	Photo : {
		init : function(oWidget, oField){
			$(oWidget.oForm[ $(oField).attr('name')+'_fPhoto']).change(function(){
				if(event.target.files && event.target.files[0]) {

					var aFiles = event.target.files;
					var img = document.createElement("img");

					img.onload = function(){
						var canvas = document.createElement('canvas');
						var ctx = canvas.getContext('2d');

						canvas.width = 150;
	 					canvas.height = 150;

	 					ctx.drawImage(this, 0, 0, 150, 150);

	 					$(oField).attr('src', canvas.toDataURL());
					}

		 			for(var i=0,f; f=aFiles[i];i++) {
		 				if(!f.type.match('image.*')) continue;

		 				var reader = new FileReader();
		 				reader.onload = function (e) {
				        	img.src = e.target.result;
				        }
				        reader.readAsDataURL(event.target.files[0]);
		 			}
				}
			});

			$(oField).click(function(){
				$(oWidget.oForm[ $(oField).attr('name')+'_fPhoto' ]).trigger('click');
			});
		},
		set: function(oField,aRequests,aRecords) {

			if(!aRecords[ aRecords.length - 1 ]['aItem'][ $(oField).attr('name') ]) return false;

			var img = document.createElement('img');

			img.onload = function(){
				var canvas = document.createElement('canvas');
				var ctx = canvas.getContext('2d');

				canvas.width = this.width;
				canvas.height = this.height;

				ctx.drawImage(this, 0, 0);

				$(oField).attr('src',canvas.toDataURL());
			}
			img.src = 'assets/files/uploads/'+aRecords[ aRecords.length - 1 ]['aItem'][ $(oField).attr('name') ]+'?rand='+ new Date().getTime();
		},
		get: function(oField) {
			return $(oField).attr('src');
		},
		reset : function(oWidget,oField) {
			return $(oField).attr('src', $(oField).attr('field-default') || '' );
		}
	},

	/** tinyMCE Widget **/
	Editor : {
		init : function(oWidget,oField){
			var sField = oWidget.sForm + ' form [field-type="Editor"][name="'+ $(oField).attr('name') +'"]';

			tinyMCE.init({
				selector				: sField,
				height					: 300,
				init_instance_callback	: function (oEditor) {
					$(oField).get(0).Editor	= oEditor;
				}
			});
		},
		set : function(oField,aRequests,aRecords)
		{
			oField.get(0).Editor.setContent( aRecords[ aRecords.length - 1 ]['aItem'][ $(oField).attr('name') ] );
			return tinyMCE.triggerSave();

		},

		get : function(oField)
		{
			tinyMCE.triggerSave();

			return oField.val();
		},

		reset : function(oWidget,oField) {
			oField.get(0).Editor.setContent( oField.attr('field-type') || '' );
			return tinyMCE.triggerSave();
		}

	},
	Default : {
		init : function(oWidget,oField){
			return $(oField).val( $(oField).attr('field-default') || '' );
		},
		/* Set Value */
		set : function(oField,aRequests,aRecords)
		{
			return $(oField).val( aRecords[ aRecords.length - 1 ]['aItem'][ $(oField).attr('name') ] );
		},
		get : function(oField)
		{
			return $(oField).val();
		},
		reset : function(oWidget,oField){
			$(oField).val( $(oField).attr('field-default') || ''  );
		}
	},

	Select : {
		init : function(oWidget,oField){
			$(oField).select2();
		},
		populate : function(oField,aData){
			var sOptions = '<option></option>';
			$.each(aData, function(iKey,sValue){
				sOptions+= '<option value="'+ iKey +'">'+ sValue +'</option>';
			});

			$(oField).empty().append(sOptions).val('').trigger('change');
		},
		set : function(oField,aRequests,aRecords){
			return $(oField).val( aRecords[ aRecords.length - 1 ]['aItem'][ $(oField).attr('name') ] ).trigger('change');
		},
		get: function(oField){
			return oField.val() || null;
		},
		reset : function(oWidget,oField){
			$(oField).val( $(oField).attr('field-default') || ''  ).trigger('change');
		}
	},
	Date : {
		init : function(oWidget,oField){

			$(oField).datetimepicker({
		        format: oField.attr('widget-date-format') ? oField.attr('widget-date-format') : 'M dd, yyyy',
		        autoclose: true,
		        todayBtn: true,
				minView: 2
		    });
		},

		set : function(oField,aRequests,aRecords){
			var sVal = aRecords[ aRecords.length - 1 ]['aItem'][ $(oField).attr('name') ];
			var sFormat = oField.attr('widget-date-format') ? oField.attr('widget-date-format') : 'M d, Y';

			return $(oField).val( (sVal > 0 && sVal != null) ? oPhp.date(sFormat,sVal) : null);
		},

		get: function(oField){
			return oPhp.strtotime(oField.val());
		},

		reset : function(oWidget,oField){
			$(oField).val( $(oField).attr('field-default') || ''  ).trigger('change');
		}
	},
	Time : {
		init : function(oWidget,oField){
			return $(oField).timepicker({
				showInputs: false,
				minuteStep: 15
			});
		},
		set: function(oField,aRequests,aRecords){
			return $(oField).timepicker( 'setTime',
				aRecords[ aRecords.length - 1 ]['aItem'][ $(oField).attr('name') ] ?
					oPhp.date('h:i A', aRecords[ aRecords.length - 1 ]['aItem'][ $(oField).attr('name') ] ) :
					oPhp.date('h:i A', new Date())
				);
		},
		get: function(oField){
			return oPhp.strtotime(oPhp.date('Y m d',new Date())+' '+$(oField).val())
		},
		reset: function(oWidget,oField){

		}
	},
	Radio : {
		set : function(oField,aRequests,aRecords){

		},

		get: function(oField){
			return $(oField).filter(':checked').val();
		},
	}
};

window.oModals = {
	init : function(oWidget) {
		if(oWidget.oForm) return;

		oWidget.oForm = $(oWidget.sForm).find('form').get(0);

		$(oWidget.oForm).submit( function(){
			if($(this).valid()) {
				oWidget.save(this,function(){
					oWidget.oModal.modal('hide');
					oWidget.aDatatable && oTables.reload(oWidget);
				});
			} else {
				toastr['error']('Please check your entry.','Validation Error');
			}
		});

		oWidget.oModal = $(oWidget.sForm).modal({
			keyboard: false,
			backdrop: 'static',
			show: false
		}).on('hidden.bs.modal',function(){
			oForms.reset( oWidget );
			oWidget.aDatatable && oTables.reload(oWidget);
		});

		$(oWidget.oForm).validate(oWidget.aValidation);
	},
	confirm : function(oNode,oForm,fCb){

		oForm.find('.modal-body').empty().append( $(oNode).attr('action').substr(0,1).toUpperCase() + $(oNode).attr('action').substr(1) +' this record?');
		oForm.find('button[name="Confirm"]').unbind().click( function(){
			fCb && fCb(oForm);
		});

		oForm.modal({
			keyboard: false,
			backdrop: 'static'
		});
	}
};


window.oModules = {

	init : function(){

	},
	load : function(aArg)
	{
		console.log(aArg);
		oModules[ aArg.sModule ] && oModules[ aArg.sModule ].aWidgets.init(aArg,function(){
			$('div.lists').find('div.row[role="list"]').css('display','none');
			$('div[name="'+ aArg.sModule +'"][role="list"]').css('display','block');
		});
	},
	request : function(oWidget,aArg,aRequest,fCb)
	{
		if(aRequest[0].length > 0 || aArg.sId){
			HTTP.request('get',(oWidget.sModuleName).toLowerCase() + (aArg.sId ? ('/'+aArg.sId) : ''),{
				aRequests : aRequest[0]
			},function(aResp){
				if(aResp.iError) {
					toastr['error'](aResp.sMessage,'Server Error')
				} else if(aResp.recordsTotal == 0) {
					toastr['error']('Please try again.','No Record found')
				} else {
					oForms.set(oWidget,aArg,aRequest,aResp);
					oWidget.oModal.modal('show');
					fCb && fCb(aResp);
				}
			});
		} else {
			oWidget.oModal.modal('show');
		}

	},
	reload : function(oModule)
	{
		//Reset forms
		//reload datatables
	}

};

window.HTTP = {
	aConfig : {
		sUrl : window.location.href+'api/',
	},
	request : function(sMethod,sModule,aRequest,fCb){
		$.ajax({
			url: (this.aConfig.sUrl+sModule).toLowerCase(),
			type		: sMethod,
			dataType	: 'text',
			processData	: false,
			timeout		: this.iTimeOut,
			data 		: aRequest ? window.btoa(JSON.stringify(aRequest)) : null,
			beforeSend: function(){
				$('.loader').css('display','block');
			},
			success 	: function(aResp){
				var aData = JSON.parse(window.atob(aResp));
				fCb && fCb(aData);

 	 			$('.loader').css('display','none');
			},
			error 		: function(){
				toastr['error']('There are some error in your applicaiton. Please contact the administrator.','System Error')
				$('.loader').css('display','none');
			}
		});
	}
}


window.oData = {};
