window.oModules.Riders = {
	aWidgets : {
		init : function(aArg,fCb){
			this.Riders.init(aArg,fCb);
		},
		Riders : {
			bInit : false,
			sPath : 'Riders/Riders',
			sModuleName	: 'Riders',
			sForm : 'div[name="Riders"][role="dialog"]',
			sList : 'div[name="Riders"][role="list"]',
			aDatatable: {
				sName		: 'Riders_DataTable',
				sTable		: 'users',
				sIndex		: 'id',
				aColumns 	: [
					{
						sTitle 		: 'ID',
						mData		: 'id',
						sWidth		: 30,
						render		: function(data, type, row) {
							return (row.is_active == 1 ? '<i class="fa fa-check" style="color:green"></i> ' : '<i class="fa fa-times"  style="color:red"></i> ')+data;
						}
					},
					{
						mData		: 'photo',
						bVisible 	: false,
					},
					{
						sTitle 		: 'Name',
						mData		: 'fullname'
					},
					{
						sTitle 		: 'Username',
						mData		: 'username'
					},
					{
						sTitle 		: 'Role',
						mData		: 'role'
					},
					{
						sTitle 		: 'Date Added',
						mData		: 'created_at',
						render 		: function(data,type,row){
							return oPhp.date('M d, Y',data);
						}
					},
					{
						sTitle 		: 'Last Update',
						mData		: 'updated_at',
						render 		: function(data,type,row){
							return data ? oPhp.date('M d, Y',data) : '';
						}
					}
				]

			},
			aValidation :
			{
				rules : {
					id					: { },

					username			: { required: true, maxlength: 32 },
					password 			: { requiredIfAdd: true, maxlength: 32 },

					last_name 			: { required: true, maxlength: 32 },
					first_name 			: { required: true, maxlength: 32 },
					middle_name 		: { maxlength: 32 },

					birthdate 			: { requiredIfSelf: true },
					gender 				: { requiredIfSelf: true },
					contact_number 		: { requiredIfSelf: true, maxlength: 13 },

					photo				: { required: true, maxlength: 255 },
					photo_motor			: { required: true, maxlength: 255 },
					license_number		: { required: true, maxlength: 15 },
					plate_number		: { required: true, maxlength: 6 },
					or_cr				: { required: true, maxlength: 20 }
				}
			},

			init : function(aArg,fCb){
				var oSelf = this;
				if(this.bInit) {
					oTables.reload(this);
				} else {
					oTables.init(this,aArg);
					oForms.init(this);

					$.validator.addMethod('requiredIfAdd',function(sVal){
						return !$(oSelf.oForm.id).val() && !sVal ? false : true;
					},'This field is required.');

					$.validator.addMethod('requiredIfSelf',function(sVal){
						return $(oSelf.oForm.id).val() == oData.user_id && !sVal ? false : true;
					},'This field is required.');

					this.bInit = true;
				}

				fCb && fCb();
			},

			forms : function(aArg,fCb) {

				var oSelf = this;
				var aRequest = [
					[],[]
				];

				oModules.request(this, aArg,aRequest, function(aResp){
					console.log(aResp)
					fCb && fCb(aResp);

					$(oSelf.oForm.password).val('');
					$(oSelf.oForm.role_id).val( aResp[ aResp.length - 1 ].aItem.role_id ).trigger('change');
				});
			},
			save : function(oNode){
				app.aActions.save(this,function(aResp){
					console.log(aResp);
				});
			}
		}
	}
}
