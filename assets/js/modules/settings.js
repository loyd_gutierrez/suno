window.oModules.Settings = {
	aWidgets : {
		init : function(aArg,fCb){
			this.Settings.init(aArg,fCb);
		},
		Settings : {
			bInit : false,
			sPath : 'Settings/Settings',
			sModuleName	: 'Settings',
			sForm : 'div[name="Settings"][role="dialog"]',
			sList : 'div[name="Settings"][role="list"]',
			aValidation :
			{
				rules : {
					username			: { required: true, maxlength: 32 },
					password 			: { maxlength: 32 },
					minimumRate			: {},
					perKM				: {}
				}
			},

			init : function(aArg,fCb){
				var oSelf = this;

				HTTP.request('get','settings',null,function(aResp){
					$(oSelf.sList+ ' input[name="username"]').val(aResp[0].aItem.username);
					$(oSelf.sList+ ' input[name="perKM"]').val(parseFloat(aResp[0].aItem.perKM).toFixed(2));
					$(oSelf.sList+ ' input[name="minimumRate"]').val(parseFloat(aResp[0].aItem.minimumRate).toFixed(2));
				});

				oSelf.aDatatable = { sIndex : 1 };

				if(this.bInit) {

				} else {
					$(oSelf.sList+ ' button[name="editButton"]').click(function(){
						oTables.action(this, oSelf.sPath);
					})

					oForms.init(this);

					$.validator.addMethod('requiredIfAdd',function(sVal){
						return !$(oSelf.oForm.id).val() && !sVal ? false : true;
					},'This field is required.');

					$.validator.addMethod('requiredIfSelf',function(sVal){
						return $(oSelf.oForm.id).val() == oData.user_id && !sVal ? false : true;
					},'This field is required.');

					this.bInit = true;
				}

				fCb && fCb();
			},

			forms : function(aArg,fCb) {

				var oSelf = this;
				var aRequest = [[],[]];

				aArg.sId = 1;

				oModules.request(this, aArg ,aRequest, function(aResp){
					fCb && fCb(aResp);

					$(oSelf.oForm.password).val('');
					$(oSelf.oForm.role_id).val( aResp[ aResp.length - 1 ].aItem.role_id ).trigger('change');
				});
			},
			save : function(oNode){
				app.aActions.save(this,function(aResp){
					console.log(aResp);
				});
			}
		}
	}
}
