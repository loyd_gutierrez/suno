window.oModules.Users = {
	aWidgets : {
		init : function(aArg,fCb){
			this.Users.init(aArg,fCb);
			console.log(oData);
		},
		Users : {
			bInit : false,
			sPath : 'Users/Users',
			sModuleName	: 'Users',
			sForm : 'div[name="Users"][role="dialog"]',
			sList : 'div[name="Users"][role="list"]',
			aDatatable: {
				sName		: 'Users_DataTable',
				sTable		: 'users',
				sIndex		: 'id',
				aColumns 	: [
					{
						sTitle 		: 'ID',
						mData		: 'id',
						sWidth		: 30,
						render		: function(data, type, row) {
							return (row.is_active == 1 ? '<i class="fa fa-check" style="color:green"></i> ' : '<i class="fa fa-times"  style="color:red"></i> ')+data;
						}
					},
					{
						mData		: 'photo',
						bVisible 	: false,
					},
					{
						sTitle 		: 'Name',
						mData		: 'fullname'
					},
					{
						sTitle 		: 'Username',
						mData		: 'username'
					},
					{
						sTitle 		: 'Role',
						mData		: 'role'
					},
					{
						sTitle 		: 'Date Added',
						mData		: 'created_at',
						render 		: function(data,type,row){
							return oPhp.date('M d, Y',data);
						}
					},
					{
						sTitle 		: 'Last Update',
						mData		: 'updated_at',
						render 		: function(data,type,row){
							return data ? oPhp.date('M d, Y',data) : '';
						}
					}
				]

			},
			aValidation :
			{
				rules : {
					photo 				: { },
					id					: { },
					role_id				: { },
					department_id 		: { },

					username			: { required: true, maxlength: 32 },
					password 			: { requiredIfAdd: true, maxlength: 32 },

					last_name 			: { required: true, maxlength: 32 },
					first_name 			: { required: true, maxlength: 32 },
					middle_name 		: { maxlength: 32 },

					birthdate 			: { requiredIfSelf: true },
					gender 				: { requiredIfSelf: true },

					contact_number 		: { requiredIfSelf: true },
					zipcode_id 			: { requiredIfSelf: true },
					address 			: { requiredIfSelf: true },

				}
			},

			init : function(aArg,fCb){
				var oSelf = this;
				if(this.bInit) {
					oTables.reload(this);
				} else {
					oTables.init(this,aArg);
					oForms.init(this);

					$.validator.addMethod('requiredIfAdd',function(sVal){
						return !$(oSelf.oForm.id).val() && !sVal ? false : true;
					},'This field is required.');

					$.validator.addMethod('requiredIfSelf',function(sVal){
						return $(oSelf.oForm.id).val() == oData.user_id && !sVal ? false : true;
					},'This field is required.');

					$(oSelf.oForm.zipcode_id).change(function(){
						if(!$(this).val()) return;

						HTTP.request('get', 'zipcodes/'+$(this).val(),{},function(aResp){
							$(oSelf.oForm.province).val(aResp[0].aItem.province);
							$(oSelf.oForm.municipality).val(aResp[0].aItem.municipality);
							$(oSelf.oForm.district).val(aResp[0].aItem.district);
						});
					});

					this.bInit = true;
				}

				fCb && fCb();
			},

			forms : function(aArg,fCb) {

				var oSelf = this;
				var aRequest = [
					[
						{
							sModule: 'roles',
							sIndex: 'id',
							sColumn: 'role',
							aColumns : [
								'id',
								'role'
							]
						},
						{
							sModule: 'zipcodes',
							sIndex: 'id',
							sColumn: 'zipcode',
							aColumns : [
								'id',
								'zipcode'
							]
						},
					],[
						'role_id',
						'zipcode_id'
					]
				];

				oModules.request(this, aArg,aRequest, function(aResp){
					console.log(aResp)
					fCb && fCb(aResp);

					$(oSelf.oForm.password).val('');
					$(oSelf.oForm.role_id).val( aResp[ aResp.length - 1 ].aItem.role_id ).trigger('change');
					$(oSelf.oForm.zipcode_id).val( aResp[ aResp.length - 1 ].aItem.zipcode_id ).trigger('change');
				});
			},
			save : function(oNode){
				app.aActions.save(this,function(aResp){
					console.log(aResp);
				});
			}
		}
	}
}
