window.oModules.Account = {
	aWidgets : {
		init : function(aArg,fCb){
			this.Account.init(aArg,fCb);
		},
		Account : {
			bInit : false,
			sPath : 'Account/Account',
			sModuleName	: 'Account',
			sForm : 'div[name="Account"][role="dialog"]',
			sList : 'div[name="Account"][role="list"]',
			aDatatable: {
				sName		: 'Account_DataTable',
				sTable		: 'users',
				sIndex		: 'id',
				aColumns 	: [
					{
						sTitle 		: 'ID',
						mData		: 'id',
						sWidth		: 30,
						render		: function(data, type, row) {
							return (row.is_active == 1 ? '<i class="fa fa-check" style="color:green"></i> ' : '<i class="fa fa-times"  style="color:red"></i> ')+data;
						}
					},
					{
						sTitle 		: 'Name',
						mData		: 'fullname'
					},
					{
						sTitle 		: 'Username',
						mData		: 'username'
					},
					{
						sTitle 		: 'Date Added',
						mData		: 'created_at',
						render 		: function(data,type,row){
							return oPhp.date('M d, Y',data);
						}
					},
					{
						sTitle 		: 'Last Update',
						mData		: 'updated_at',
						render 		: function(data,type,row){
							return data ? oPhp.date('M d, Y',data) : '';
						}
					}
				]

			},
			aValidation :
			{
				rules : {
					id					: { },

					username			: { required: true, maxlength: 32 },
					password 			: { requiredIfAdd: true, maxlength: 32 },
				}
			},

			init : function(aArg,fCb){
				var oSelf = this;
				if(this.bInit) {
					oTables.reload(this);
				} else {
					oTables.init(this,aArg);
					oForms.init(this);

					$.validator.addMethod('requiredIfAdd',function(sVal){
						return !$(oSelf.oForm.id).val() && !sVal ? false : true;
					},'This field is required.');

					$.validator.addMethod('requiredIfSelf',function(sVal){
						return $(oSelf.oForm.id).val() == oData.user_id && !sVal ? false : true;
					},'This field is required.');

					this.bInit = true;
				}

				fCb && fCb();
			},

			forms : function(aArg,fCb) {

				var oSelf = this;
				var aRequest = [
					[
						{
							sModule: 'roles',
							sIndex: 'id',
							sColumn: 'role',
							aColumns : [
								'id',
								'role'
							]
						}
					],[
						'role_id'
					]
				];

				oModules.request(this, aArg,aRequest, function(aResp){
					console.log(aResp)
					fCb && fCb(aResp);

					$(oSelf.oForm.password).val('');
					$(oSelf.oForm.role_id).val( aResp[ aResp.length - 1 ].aItem.role_id ).trigger('change');
				});
			},
			save : function(oNode){
				app.aActions.save(this,function(aResp){
					console.log(aResp);
				});
			}
		}
	}
}
