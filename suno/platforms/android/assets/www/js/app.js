var driverMarker = null;
var pickupMarker = null;
var googleMap = null;
var autocompvare = null;
var directionsService = null;

$(document).ready(function(){
    window.auth.init();
    app.links.init();

    $("span.toggleMenu").click(function(){
        $("#mySidenav").toggleClass("displayBlock");
         $("#main").toggleClass("addMargin");
    });

     $("button.search").click(function(){
         $("#mainsearch").toggleClass("spanSearch");
         $("#mainsearch .mainsearch-body label").toggleClass("hideShow");
         $("#mainsearch .mainsearch-body input").toggleClass("hideShow");
         $("#mainsearch .mainsearch-body input").toggleClass("addWidth");
        // $("span.toggleMenu").toggleClass("hideShow");
    });

    $('.locationPanel').click(function(){
        if(app.bookedId) return;
        if((app.aLogged.role).toLowerCase() === 'rider') return;
        if(this.id !== 'startLocation' && !app.map.pickup) return alert('select pick up first!');

        app.map.state = this.id == 'startLocation' ? 'pickup' : 'dropoff';

        $('#locationModal').modal('show');

        $('input[name="pickup"]').val( app.map[app.map.state] ? app.map[app.map.state].name : '' );
    })

    // $('#contact_number_panel').click(function(){
    //     window.plugins.CallNumber.callNumber(function(){
    //         console.log('call success');
    //     }, function(err){
    //         console.log("Error:"+err);
    //     }, $(this).attr('data'), true);
    // });

    $('#locationModal').on('shown.bs.modal',function(){
        $('input[name="pickup"]').focus();
    });

    $.each($('img[name="photo"]'), function(){

        $(this).siblings('input[name="photo_fPhoto"]').change(function(){

            var oFile = this;
            if(event.target.files && event.target.files[0]) {
                var aFiles = event.target.files;
                var img = document.createElement("img");

                img.onload = function(){
                    var canvas = document.createElement('canvas');
                    var ctx = canvas.getContext('2d');

                    canvas.width = 150;
                    canvas.height = 150;

                    ctx.drawImage(this, 0, 0, 150, 150);

                    $(oFile).siblings('img[name="photo"]').attr('src', canvas.toDataURL());
                }

                for(var i=0,f; f=aFiles[i];i++) {
                    if(!f.type.match('image.*')) continue;

                    var reader = new FileReader();
                    reader.onload = function (e) {
                        img.src = e.target.result;
                    }
                    reader.readAsDataURL(event.target.files[0]);
                }
            }
        });

        $(this).click(function(){
            $(this).siblings('input[name="photo_fPhoto"]').trigger('click');
        })
    });
});

window.rider = {
    isCoordLogged: false,
    isBooked: false,
    tableContainer: null,
    isCoordSending: false,
    init : function(){
        this.tableContainer = $('.booking-view');
        this.isBooked = false;

        var oSelf = this;

        $('.map-view').css('display', 'none' );
        $('#btn-bookCustomer').html('Book');
        $('#btn-arrived').attr('disabled',true);
        $('.booking-view').css('display', 'block');

        !oSelf.isCoordLogged && HTTP.request('GET', 'mobile/coords/' + app.aLogged.id, [], function(aResp){
            oSelf.isCoordLogged = aResp[0].aItem.status ? true : false;
            oSelf.sendCoord();
        });
    },
    sendCoord: function(){
        if(app.aLogged.length == 0) return;
        if(this.isCoordSending) return false;
        this.isCoordSending = true;

        var oSelf = this;
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position){
                if(app.aLogged.length == 0) return;
                // oSelf.bInit = true;
                setTimeout(function() {
                    this.isCoordSending = false;
                    oSelf.sendCoord();
                    coords = position.coords;

                    googleMap.setCenter({lat: coords.latitude, lng: coords.longitude});
                    driverMarker.setPosition({lat: coords.latitude, lng: coords.longitude});
                    driverMarker.setVisible(true);

                    HTTP.request( oSelf.isCoordLogged ? 'PUT' : 'POST', 'mobile/coords/' + app.aLogged.id, {
                        aRequest : {
                            id : app.aLogged.id,
                            lat : coords.latitude,
                            lng : coords.longitude,
                            customer_id: (rider.isBooked ? $('#customersInformation').attr('customer_id') : null)
                        }
                    }, function(aResp){
                        oSelf.isCoordLogged = true;
                        oSelf.bookRequests(aResp.data)

                        if(rider.isBooked) {
                            app.bookedId = aResp.id;

                            if( (aResp.status != 0) &&
                                aResp.status == null ||
                                aResp.status == -1 ||
                                aResp.status == 1
                            ) {
                                alert('Ride ' + (aResp.status == -1  ? 'has been cancelled.' : 'finsihed.'));
                                rider.init();
                            }
                        }

                    }, false);
                }, 2000);
            },function(err){
                console.log(err);
                oSelf.sendCoord();
            });
        } else {
            alert('please open location services on your device and restart the app')
        }
    },
    bookRequests : function(customers){
        this.tableContainer.empty();

        var temp = '';
        $.each(customers, function(key,row){

            temp+= '<div class="row" '+
                'data="'+ row.id +'" '+
                'name="'+ row.customer +'" '+
                'customer_id="'+ row.customer_id +'" '+
                'contact="'+ row.customer_contact +'"'+
                'pickup_lat="'+ row.pickup_lat +'"'+
                'pickup_lng="'+ row.pickup_lng +'"'+
                'dropoff_lat="'+ row.dropoff_lat +'"'+
                'dropoff_lng="'+ row.dropoff_lng +'"' +
                'pickup="'+ row.pickup +'"' +
                'photo="'+ row.customer_photo +'"' +
                'dropoff="'+ row.dropoff +'">' +

                '<div class="booking-location">' +
                  '<div class="booking-pickup"> <i class="fa fa-circle" aria-hidden="true"></i> &nbsp; <label> '+ row.pickup +' </label></div>' +
                  '<div class="booking-dropoff"> <i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp; <label>  '+ row.dropoff +'  </label></div>' +
                '</div>' +
                '<div class="booking-button">' +
                  '<button class="btn-bookRequest"> > </button>' +
                '</div>' +
             '</div>';
        });

        this.tableContainer.append( (customers.length > 0) ? temp : '<div class="booking-view-default"> No booking(s) found </div>');

        $(this.tableContainer).find('.row').each(function(){
            $(this).click(function(event){
                $('#customersInformation').attr({
                    'data': $(this).attr('data'),
                    'customer_id': $(this).attr('customer_id'),
                    'pickup_lat': $(this).attr('pickup_lat'),
                    'pickup_lng': $(this).attr('pickup_lng'),
                    'dropoff_lat': $(this).attr('dropoff_lat'),
                    'dropoff_lng': $(this).attr('dropoff_lng')
                });

                $('#customersInformation img#customerImage').attr('src', ('http://' + HTTP.aConfig.uploads + $(this).attr('photo')) );
                $('#customersInformation label#customerName').text( $(this).attr('name') );
                $('#customersInformation label#customerContact').text( $(this).attr('contact') );
                $('#customersInformation .customerContactNumber').attr( 'href', 'tel:'+$(this).attr('contact') );
                $('#customersInformation label#customerPickup').text( $(this).attr('pickup') );
                $('#customersInformation label#customerDropoff').text( $(this).attr('dropoff') );

                $('#customersInformation').modal('show');
            });
        });
    },
    book: function(data){
        $('h4[state="pickup"] label').html(data.pickup);
        $('h4[state="dropoff"] label').html(data.dropoff);

        directionsService.route({
            origin: data.pickup,
            destination: data.dropoff,
            travelMode: 'DRIVING'
        }, function(response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);

                distanceService.getDistanceMatrix({
                    origins: [ data.pickup ],
                    destinations: [ data.dropoff ],
                    travelMode: 'DRIVING'
                },function(response,status){
                    var resp = response.rows[0].elements[0];

                    if(status == 'OK') {
                        googleMap.setZoom(16);
                        google.maps.event.trigger(googleMap, 'resize');

                        HTTP.request('GET', 'mobile/settings/' , [] , function(aResp){
                            var settings = aResp[0].aItem;

                            $('div.book-rate-result label#book-km').empty().html(resp.distance.text);
                            $('div.book-rate-result label#book-mins').empty().html(resp.duration.text);
                            $('div.book-rate-result label#book-rate').empty().html('&#8369; '+ app.map.computeRate(resp.distance.value,settings.minimumRate, settings.perKM));

                            $('div.book-rate-result').css('display','block');
                        });
                    }
                });

            } else {

                $('h4[state="'+ state +'"] label').empty();
                window.alert('Directions request failed due to ' + status);
            }
        });
    },
    processBooked: function(data){
        var oSelf = this;
        if(data.status == 0) {
            oSelf.isBooked = true;
            app.bookedId = data.id;
            $('#customersInformation').attr('customer_id', data.customer_id);

            google.maps.event.trigger(googleMap, 'resize');
            $('.btn-holder button.btn-book').removeClass('disabled').html('CUSTOMER INFO').attr('disabled',false);

            $('#btn-arrived').attr('disabled',false);
            $('#btn-bookCustomer').html('Cancel');

            $('#customersInformation label#customerName').text( data.customer );
            $('#customersInformation label#customerContact').text( data.customer_contact );
            $('#customersInformation a.customerContactNumber').attr( 'href', 'tel:' + data.customer_contact );
            $('#customersInformation label#customerPickup').text( data.pickup );
            $('#customersInformation label#customerDropoff').text( data.dropoff );

            $('.booking-view').fadeOut(function(){
                $('.map-view').fadeIn(function(){
                    googleMap.setZoom(16);
                    google.maps.event.trigger(googleMap, 'resize');
                });
            });
        }

    },
    cancelBooking: function(){},
    reset: function(){}
};

window.customer = {
    bookingCounter: 0,
    bookingTimeout: 20,
    isBooked: false,
    isBooking: false,
    arrivalNotified: false,
    isRequestingRiderNearme: false,
    ridersNearbyMarker: [],
    timer: null,
    navigateTimer: null,
    init: function(){
        $('.map-view').css('display', 'block' );
        $('.booking-view').css('display', 'none');
        $('#riderName').html('');
        $('#riderContact').html('');

        this.getRidersNearMe();
        this.resetBooking();
        $('.btn-holder button.btn-book').removeClass('disabled').addClass('disabled').attr('disabled',true).html('BOOK');
        google.maps.event.trigger(googleMap, 'resize');
    },
    book: function(){
        var oSelf = this;
        this.bookingCounter++;

        $('.btn-holder button.btn-book').text( (oSelf.bookingCounter <= 3 ? 'CANCEL' : 'LOOKING FOR RIDER') + ' ('+ (oSelf.bookingTimeout - oSelf.bookingCounter) +')');

        if(oSelf.bookingCounter == oSelf.bookingTimeout) {
            oSelf.cancelBooking();
            return alert('Oops! No rider are available at the moment. Please try again');
        }

        setTimeout( function(){
            if(oSelf.bookingCounter === 3) {
                $('.btn-holder button.btn-book').addClass('disabled').attr('disabled',true);
                $('.loader').css('display','block');
            }

            !oSelf.isBooked && oSelf.isBooking && HTTP.request( 'POST', 'mobile/bookings/' + app.aLogged.id, {
                aRequest : {
                    customer_id : app.aLogged.id,
                    pickup: app.map.pickup.name,
                    pickup_lat : app.map.pickup.geometry.location.lat(),
                    pickup_lng : app.map.pickup.geometry.location.lng(),
                    dropoff: app.map.dropoff.name,
                    dropoff_lat : app.map.dropoff.geometry.location.lat(),
                    dropoff_lng : app.map.dropoff.geometry.location.lng(),
                    rate : $('#book-rate').text().substr(2)
                }
            }, function(aResp){
                if(aResp.data[0].rider_id) {
                    alert('We have found a rider!');

                    oSelf.resetBooking();
                    oSelf.isBooked = true;
                    app.bookedId = aResp.data[0].id;

                    $('.btn-holder button.btn-book').removeClass('disabled').html('Rider Info').attr('disabled',false);

                    if(aResp.data[0].photo) {
                        $('#ridersInformation img#riderImage').attr('src', 'http://' + HTTP.aConfig.uploads + aResp.data[0].photo);
                    }

                    $('#riderName').html(aResp.data[0].rider);
                    $('#riderContact').html(aResp.data[0].rider_contact);

                    oSelf.navigateRider(aResp.data[0].rider_id);
                }

                if( oSelf.isBooking &&
                    oSelf.bookingCounter < oSelf.bookingTimeout &&
                    !oSelf.isBooked
                ) {
                    oSelf.book();
                }
            }, false );
        } , 2000);
    },
    getRidersNearMe: function() {
        if(this.isBooked || this.isRequestingRiderNearme) return false;
        this.isRequestingRiderNearme = true;
        var oSelf = this;

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position){
                HTTP.request('GET', 'mobile/coords/', {
                    ridersNearby: true,
                    aRequest : {
                        lng: position.coords.longitude,
                        lat: position.coords.latitude
                    }
                }, function(aResp){
                    $.each(aResp[0].aItem, function(key,row){
                        var tempCoords = new google.maps.LatLng(row.lat, row.lng);
                        oSelf.ridersNearbyMarker[key] = new google.maps.Marker({
                            map: googleMap,
                            position: tempCoords,
                            visible: true,
                            icon: 'img/rider.png'
                        });
                        // oSelf.ridersNearbyMarker[key].setVisible(true);
                    });
                    if(oSelf.timer === null) {
                        oSelf.timer = setTimeout(function(){
                            oSelf.isRequestingRiderNearme = false;
                            oSelf.timer = null;
                            oSelf.getRidersNearMe();
                        }, 5000);
                    }
                }, false);
            });
        } else {
            alert('please open location services on your device and restart the app')
        }
    },
    getRiderDistance: function(rider_lat, rider_lng) {
        if(!this.isBooked) return '';
        var oSelf = this;

        var origin = new google.maps.LatLng(rider_lat, rider_lng);
        distanceService.getDistanceMatrix({
            origins: [origin],
            destinations: [ app.map.dropoff.name ],
            travelMode: 'DRIVING'
        },function(response,status){
            if(status == 'OK') {
                var resp = response.rows[0].elements[0];
                $('#riderDistance').html(resp.duration.text);
                $('#riderDuration').html(resp.distance.text);
                if(resp.distance.value < 500 && !oSelf.arrivalNotified) {
                    oSelf.arrivalNotified = true;
                    alert('Your driver is arriving. Get ready');
                }
            }
        });
    },
    navigateRider: function(rider_id){

        if(!customer.isBooked) {
            directionsDisplay.setMap(null);
            driverMarker.setVisible(false);
            alert('Transaction compvare! Thank you for booking with us.');
            return false;
        }

        var oSelf = this;
        oSelf.isBooked && HTTP.request('GET', 'mobile/coords/' + rider_id, {
            aRequest : {
                id: app.bookedId
            }
        }, function(aResp){
            $('#riderDistance').html(oSelf.getRiderDistance(aResp[0].aItem.lat,aResp[0].aItem.lng));
            driverMarker.setPosition({lat: parseFloat(aResp[0].aItem.lat), lng: parseFloat(aResp[0].aItem.lng) });
            driverMarker.setVisible(true);

            if( customer.isBooked &&
                (aResp[0].aItem.status == -1 ||
                aResp[0].aItem.status == 1)
            ) {
                alert('Ride ' + (aResp[0].aItem.status == -1  ? 'has been cancelled.' : 'finsihed.'));
                app.init();
                directionsDisplay.setMap(null);
                driverMarker.setVisible(false);
                $('#ridersInformation').modal('hide');
            }

            oSelf.navigateTimer = setTimeout(function(){
                clearTimeout(oSelf.navigateTimer);
                oSelf.navigateRider(rider_id);
            }, 2000);
        },false);
    },
    processBooked: function(data){
        this.isBooked = true;
        app.bookedId = data.id;

        $('.btn-holder button.btn-book').removeClass('disabled').html('Rider Info').attr('disabled',false);

        $('#riderName').html(data.rider);
        $('#riderContact').html(data.rider_contact);

        for(i = 0; i < this.ridersNearbyMarker.length; i++) {
            // this.ridersNearbyMarker[i].setVisible(false);
            this.ridersNearbyMarker[i].setMap(null);
        }
        this.ridersNearbyMarker = [];

        this.navigateRider(data.rider_id);
    },
    cancelBooking: function(){
        var oSelf = this;
        HTTP.request( 'DEvarE', 'mobile/bookings/' + app.aLogged.id, [], function(aResp){
            oSelf.resetBooking();
        });
    },
    resetBooking: function(){
        this.isBooked = false;
        this.isBooking = false;
        this.bookingCounter = 0;
        app.bookedId = null;
        this.arrivalNotified = false;
        this.isRequestingRiderNearme = false;

        for(i = 0; i < this.ridersNearbyMarker.length; i++) {
            this.ridersNearbyMarker[i].setMap(null);
        }

        this.ridersNearbyMarker = [];
        $('.btn-holder button.btn-book').removeClass('disabled').html('BOOK').attr('disabled',false);
        $('.loader').css('display','none');
    }
}

window.app = {
    bInit : false,
    init : function(){
        var oSelf = this;
        if(!googleMap) {
            oSelf.init();
            return false;
        }

        setTimeout(function(){
            if(googleMap) {
                var aLogged = JSON.parse(sessionStorage['aLogged']);
                oSelf.aLogged = aLogged;
                window[ (oSelf.aLogged.role).toLowerCase() ].init();

                $('div.book-rate-result').css('display','none');
                oSelf.map.init();

                HTTP.request('GET', 'mobile/transactions/', {
                    status: 1,
                    id: aLogged.id,
                    role: aLogged.role
                }, function(aResp) {
                    if(aResp[0].aItem && aResp[0].aItem.status == 0) {
                        var location = new google.maps.LatLng(aResp[0].aItem.dropoff_lat,aResp[0].aItem.dropoff_lng);

                        app.map.state = 'dropoff';
                        app.map.pickup = {
                            name: aResp[0].aItem.pickup,
                        }

                        window[ (aLogged.role).toLowerCase() ].processBooked(aResp[0].aItem);
                        app.map.setMarker(location, {
                            name: aResp[0].aItem.dropoff,
                        } ,'dropoff',function(){
                            google.maps.event.trigger(googleMap, 'resize');
                        });
                    }
                });
            }
        }, 1000);
    },
    map : {
        coords : {},
        // pickupMarker : null,
        // driverMarker : null,
        // googleMap : null,
        // autocompvare: null,
        state: null,
        pickup: null,
        dropoff: null,
        init : function(){
            this.reset();
            var oSelf = this;

            if(!googleMap) {
                setTimeout(function(){
                    oSelf.init();
                }, 1000);

                return false;
            }

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position){
                    // oSelf.bInit = true;
                    app.map.coords = position.coords;
                    googleMap.setCenter({lat: 14.5757137, lng: 121.0365384});
                });
            } else {
                alert('please open location services on your device and restart the app')
            }
        },

        reset : function() {
            this.coords = {};
            this.state = null;
            this.pickup = null;
            this.dropoff = null;

            driverMarker && driverMarker.setVisible(false);

            $('#startLocation h4 label').html('Pick up');
            $('#endLocation h4 label').html('Drop off');
        },
        // coordinates
        // state = pickup, dropoff
        setMarker : function(location,place,state){

            var oSelf = this;

            this[app.map.state] = place;
            $('h4[state="'+ state +'"] label').empty().append(place.name);

            if(state == 'pickup') {
                googleMap.setCenter(location);
                pickupMarker.setPosition(location);
                pickupMarker.setVisible(true);
            }

            if(app.map.pickup && app.map.dropoff) {
                pickupMarker && pickupMarker.setVisible(false);

                directionsService.route({
                    origin: app.map.pickup.name,
                    destination: app.map.dropoff.name,
                    travelMode: 'DRIVING'
                }, function(response, status) {
                    if (status === 'OK') {
                        directionsDisplay.setDirections(response);
                        $('button.btn-book').removeClass('disabled').attr('disabled',false);

                        distanceService.getDistanceMatrix({
                            origins: [ app.map.pickup.name ],
                            destinations: [ app.map.dropoff.name ],
                            travelMode: 'DRIVING'
                        },function(response,status){
                            var resp = response.rows[0].elements[0];

                            if(status == 'OK') {
                                HTTP.request('GET', 'mobile/settings/' , [] , function(aResp){
                                    var settings = aResp[0].aItem;

                                    $('div.book-rate-result label#book-km').empty().html(resp.distance.text);
                                    $('div.book-rate-result label#book-mins').empty().html(resp.duration.text);
                                    $('div.book-rate-result label#book-rate').empty().html('&#8369; '+oSelf.computeRate(resp.distance.value,settings.minimumRate, settings.perKM));

                                    $('div.book-rate-result').css('display','block');
                                });
                            }
                        });
                    } else {

                        $('h4[state="'+ state +'"] label').empty();
                        window.alert('Directions request failed due to ' + status);
                    }
                });
            }
        },

        computeRate: function(distance, minRate, perKM){
            if(distance < 1000) return minRate;

            return parseFloat(( (parseFloat(distance)/1000) * parseFloat(perKM)) + parseFloat(minRate)).toFixed(2);
        }
    },

    transactions : function(){

    },

    links : {
      init : function() {

        //   $('.btn').on('focus',function(){
        //       $(this).blur();
        //   })

        $('.btnSave').click(function(){
            var passed = true;
            var aRequest = {
                aRequest : {
                    photo : $('#accountSettings img[name="photo"]').attr('src'),
                    username : $('#accountSettings input[name="username"]').val(),
                    password: $('#accountSettings input[name="password"]').val(),
                    last_name: $('#accountSettings input[name="lastname"]').val(),
                    first_name: $('#accountSettings input[name="firstname"]').val(),
                    middle_name: $('#accountSettings input[name="middlename"]').val(),
                    gender: $('#accountSettings select[name="gender"]').val(),
                    birthdate: $('#accountSettings input[name="birthdate"]').val(),
                    contact_number: $('#accountSettings input[name="contact_number"]').val(),
                }
            };

            $.each(aRequest.aRequest,function(key,val){

                if(val == null || val == '' && key != 'password') {
                    passed = false;
                    return alert(key + ' is Required!');
                }
            });

            passed && HTTP.request('PUT', 'mobile/users/'+app.aLogged.id , aRequest , function(aResp){
                alert(aResp.sMessage);
                $('.sidebarLink[action="accounts"]').trigger('clicked');
            });
        });

        $('.btnBack').click(function(){
            $('div.module').css('display','none');
            $('div.'+$(this).attr('target')).fadeIn(100,function(){});
        });

        $('#btn-arrived').click(function(){
            HTTP.request('PUT', 'mobile/transactions/'+app.bookedId , {
                aRequest : {
                    status: 1
                }
            } , function(aResp){
                app.init();
                rider.init();
                $('#customersInformation').modal('hide');
            });
        });

          $('#btn-bookCustomer').click(function(){
              var modal = $('#customersInformation');

              if(!rider.isBooked) {
                  HTTP.request( 'PUT', 'mobile/bookings/' + modal.attr('customer_id'), {
                      bookFromRider: true,
                      aRequest : {
                          rider_id: app.aLogged.id
                      }
                  }, function(aResp){
                      if(aResp.iError) {
                          alert('Error! Book already expired.');
                          modal.modal('hide');
                          return;
                      }
                      rider.isBooked = true;
                      modal.modal('hide');

                      rider.book(aResp.data[0]);

                      $('#btn-arrived').attr('disabled',false);
                      $('#btn-bookCustomer').html('Cancel');

                      $('.booking-view').fadeOut(function(){
                          $('.map-view').fadeIn(function(){
                              google.maps.event.trigger(googleMap, 'resize');
                              $('.btn-holder button.btn-book').removeClass('disabled').html('CUSTOMER INFO').attr('disabled',false);
                          });
                      });
                  });
              } else {
                  var reason = prompt("Please provide a reason for cancelling this ride.");
                  if (reason != null) {
                      HTTP.request('PUT', 'mobile/transactions/'+app.bookedId , {
                          aRequest : {
                              status: -1,
                              customer_remarks: reason
                          }
                      } , function(aResp){
                          app.init();
                          rider.init();
                          $('#customersInformation').modal('hide');
                      });
                  }
              }
          });

          $('button.btn-book').click(function(){
              if(app.aLogged.role == 'Customer') {
                  if(customer.isBooking) {
                      // looking for a rider
                      customer.cancelBooking();

                  } else if(customer.isBooked) {
                      // waiting for the rider
                      $('#ridersInformation').modal('show');

                  } else {
                      // start of transaction
                      customer.isBooking = true;
                      if($(this).hasClass('disabled')) return false;
                      $('.btn-holder button.btn-book').removeClass('disabled').html('CANCEL ('+ customer.bookingTimeout +')').attr('disabled',false);

                      customer.book();
                  }
              } else {
                  if(rider.isBooked) {
                      $('#customersInformation').modal('show');
                  }
              }
          });

          $('#iHaveSafelyArrive').click(function(){
              HTTP.request('PUT', 'mobile/transactions/'+app.bookedId , {
                  aRequest : {
                      status: 1
                  }
              } , function(aResp){
                  app.init();
                  $('#ridersInformation').modal('hide');
              });
          });

          $('#cancelRide').click(function(){
              $('#cancelRide').blur();

              var reason = prompt("Please provide a reason for cancelling this ride.");
              if (reason != null) {
                  HTTP.request('PUT', 'mobile/transactions/'+app.bookedId , {
                      aRequest : {
                          status: -1,
                          customer_remarks: reason
                      }
                  } , function(aResp){
                      app.init();
                      $('#ridersInformation').modal('hide');
                  });
              }
          });

          $('a').each(function(){
              var link = $(this);
              link.attr('href','javascript:void(0)');

              if(link.hasClass('sidebarLink')) {
                  $(this).click(function(){

                      $("span.toggleMenu").trigger('click');

                      switch(link.attr('action')) {
                          case 'logout' :

                              modals.confirm.toggle(true,function(){
                                  sessionStorage.clear();
                                  app.aLogged = [];

                                  $('#showLogin').click();
                              },'Logout your account?');

                          break;

                          case 'transactions' :
                              $("#mySidenav").removeClass("displayBlock");
                              $("#main").removeClass("addMargin");

                              $(".main").css('display','none')
                              $('div.history').css('display','block');
                          break;

                          case 'accounts' :

                            var aLogged = JSON.parse(sessionStorage.aLogged);
                            HTTP.request('GET', 'mobile/users/'+aLogged.id , [] , function(aResp){
                                if(aResp.iError) {
                                    return alert( aResp.sMessage );
                                }

                                var oForm = $('div#accountSettings form');
                                var account = aResp[0].aItem;

                                $(oForm).find('img[name="photo"]').attr('src', 'http://' + HTTP.aConfig.uploads + account.photo);
                                $(oForm).find('input[name="lastname"]').val(account.last_name);
                                $(oForm).find('input[name="firstname"]').val(account.first_name);
                                $(oForm).find('input[name="middlename"]').val(account.middle_name);
                                $(oForm).find('input[name="contact_number"]').val(account.contact_number);
                                $(oForm).find('input[name="birthdate"]').val(oPhp.date('Y-m-d',account.birthdate));
                                $(oForm).find('input[name="username"]').val(account.username);

                                $("#mySidenav").removeClass("displayBlock");
                                $("#main").removeClass("addMargin");

                                $(".main").css('display','none')
                                $('div.account').css('display','block');


                            });
                          break;
                      }
                  });
              }
          });
      }
  },
}

window.HTTP = {
    aConfig : {
        // sUrl: 'localhost/suno-admin/api/',
        // uploads: 'localhost/suno-admin/assets/files/uploads/',
        sUrl: '192.168.8.100/suno-admin/api/',
        uploads: '192.168.8.100/suno-admin/assets/files/uploads/'
    },
	request : function(method, moduleName, request,fCb, showLoading){
      var isLoading = (showLoading === undefined || showLoading === null) ? true : false;

      $.ajax({
  			url: 'http://'+(this.aConfig.sUrl+ moduleName ).toLowerCase(),
  			type		: method,
  			dataType	: 'text',
  			processData	: false,
  			timeout		: this.iTimeOut,
  			data 		: request ? window.btoa(JSON.stringify(request)) : null,
  			beforeSend: function(){
  				isLoading && $('.loader').css('display','block');
  			},
  			success 	: function(aResp){
  				var aData = JSON.parse(window.atob(aResp));
  				fCb && fCb(aData);

   	 			isLoading && $('.loader').css('display','none');
  			},
  			error 		: function(){
  				alert('There are some error in your applicaiton. Please contact the administrator.');

  				isLoading && $('.loader').css('display','none');
  			}
  		});
    }
}

window.modals = {
    confirm : {
        toggle: function(state, fCb, message){
            var confirmModal = $('#confirmationModal');
            if(state) {
                $(confirmModal).find('.modal-body').html(message);
            }

            confirmModal.modal(state ? 'show' : 'hide').find('#modal-confirm').click(function(){
                modals.confirm.toggle(false);
                fCb && fCb();
            });
        },
        confirm: function(){

        }
    }
}

function initMap() {
    $('.loader').css('display','block');
    var modal = $('#locationModal');
    var pickup = document.getElementById('pickup');

    google.maps.event.addDomListener(window, "load", function(){
        console.log('asd');
        var tempCoords = new google.maps.Point(13.9419, 121.1644);

        googleMap = new google.maps.Map(document.getElementById('map'), {
            zoom: 16,
            center: {lat: 13.9419, lng: 121.1644},
            zoomControl: false,
            streetViewControl: false,
            fullscreenControl: false,
            mapTypeControl: false
        });

        driverMarker = new google.maps.Marker({
            map: googleMap,
            anchorPoint: tempCoords,
            icon: 'img/rider.png'
        });

        driverMarker.setVisible(false);

        pickupMarker = new google.maps.Marker({
            map: googleMap,
            anchorPoint: tempCoords,
            icon: 'img/pickup.png'
        });

        pickupMarker.setVisible(false);

        /** Autocompvare **/
        autocompvare = new google.maps.places.Autocomplete(pickup);
        autocompvare.bindTo('bounds', googleMap);

        var infowindow = new google.maps.InfoWindow();

        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);

        autocompvare.addListener('place_changed',function(){

            modal.modal('hide');
            infowindow.close();

            var place = autocompvare.getPlace();

            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                googleMap.fitBounds(place.geometry.viewport);
            }
            app.map.setMarker(place.geometry.location, place, app.map.state);
        });

        /** Direction **/
        directionsService = new google.maps.DirectionsService;
        directionsDisplay = new google.maps.DirectionsRenderer;

        directionsDisplay.setMap(googleMap);

        /** Distance **/
        distanceService = new google.maps.DistanceMatrixService();

        window.auth.init();
        $('.loader').css('display','none');
    });
}
