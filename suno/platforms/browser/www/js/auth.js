window.auth = {
    fadeCount: 500,
    //initialize login functions here
    init: function(){
        var oSelf = this;

        var oSignupForm = $('div.signup form');
        var oLoginForm = $('div.login form');

        // singup
        $('#showRegister').unbind().click(function(){
            $('div.login').fadeOut(auth.fadeCount,function(){

                $(oSignupForm).find('input[name="lastname"]').val("");
                $(oSignupForm).find('input[name="firstname"]').val("");
                $(oSignupForm).find('input[name="middlename"]').val("");
                $(oSignupForm).find('input[name="birthdate"]').val("");
                $(oSignupForm).find('input[name="username"]').val("");
                $(oSignupForm).find('input[name="password"]').val("");

                $('div.signup').fadeIn(100,function(){});
            });
        });

        $('#registerButton').unbind().click(function(e){
            e.preventDefault();
            oSelf.register(oSignupForm);
        });

        // login
        $('#showLogin').unbind().click(function(){
            $('div.signup').fadeOut(auth.fadeCount,function(){

                $('div.main').css('display','none');
                $(oLoginForm).find('input[name="username"]').val("");
                $(oLoginForm).find('input[name="password"]').val("");

                $('div.login').fadeIn(100,function(){});

            });
        });

        $('#loginbtn').unbind().click(function(e){
            e.preventDefault();
            oSelf.login(oLoginForm);
        });

        if(sessionStorage.aLogged) {
            $('div.login').css('display','none');
            $('div.main').css('display','block');

            app.init();
            app.map.init();

        } else {
            $('#showLogin').click();
        }
    },

    register : function(oSignupForm){
        var aRequest = {
            aRequest : {
                last_name    : $(oSignupForm).find('input[name="lastname"]').val(),
                first_name   : $(oSignupForm).find('input[name="firstname"]').val(),
                middle_name  : $(oSignupForm).find('input[name="middlename"]').val(),
                contact_number  : $(oSignupForm).find('input[name="contact_number"]').val(),
                gender      :  $(oSignupForm).find('select[name="gender"]').val(),
                birthdate   : oPhp.strtotime($(oSignupForm).find('input[name="birthdate"]').val()),
                username    : $(oSignupForm).find('input[name="username"]').val(),
                password    : $(oSignupForm).find('input[name="password"]').val()
            },
            sModule: 'customers'
    	};

        if(
            !$(oSignupForm).find('input[name="lastname"]').val() ||
            !$(oSignupForm).find('input[name="firstname"]').val() ||
            !$(oSignupForm).find('input[name="middlename"]').val() ||
            !$(oSignupForm).find('input[name="contact_number"]').val() ||
            !$(oSignupForm).find('select[name="gender"]').val() ||
            !$(oSignupForm).find('input[name="birthdate"]').val() ||
            !$(oSignupForm).find('input[name="username"]').val() ||
            !$(oSignupForm).find('input[name="password"]').val()
        ) {
            alert('Please complete your fields');
            return false;
        }

        HTTP.request('POST', 'customers', aRequest, function(aResp){
            if(aResp.iError) {
                return alert( aResp.sMessage);
            }

            alert('Account created!');
            return $('#showLogin').click();
    	});
    },

    login : function(oLoginForm){

        var aRequest = {
    		username : $(oLoginForm).find('input[name="username"]').val(),
    		password : $(oLoginForm).find('input[name="password"]').val()
    	};

    	$(oLoginForm).find('input[name="password"]').val("");

    	HTTP.request('POST','auth',aRequest,function(aResp){
            if(aResp.iError) {
                alert( aResp.sMessage);
            } else {
                if(aResp.role == 'Administrator') {
                    alert('Administrator is not allowed in this app.');
                    return false;
                }

                sessionStorage['aLogged'] = JSON.stringify(aResp);

                $('div.login').fadeOut(auth.fadeCount,function(){
                    $('div.main').fadeIn(100,function(){
                        app.init();
                        app.map.init();
                        google.maps.event.trigger(googleMap, 'resize');
                    });
                });
            }
    	});
    }
}
